;Fake level mode 0E, sprite priority  
;  
;fixed SSP  
;No layer 3  settings

main:

;invinsible
	LDA #$7F
	STA $78



   LDY #$00             ; | Y will be the loop counter.
   LDX #$00             ; | X the index for writing the table to the RAM
   LDA $13              ; | Speed of waves
   LSR #4               ; | Slowing down A
   STA $00              ;/  Save for later use.

   PHB : PHK : PLB      ;\  Preservev bank
.Loop:                  ; | Jump back if not finished writing table
   LDA #$0A             ; | Set scanline height
   STA $7F9E00,x        ; | for each wave
   TYA                  ; | Transfer Y to A, to calculate next index
   ADC $00              ; | Add frame counter
   AND #$0F             ; | 
   PHY                  ; | Preserve loop counter
   TAY                  ;/  Get the index in Y

   LDA.w .WaveTable,y   ;\  Load in wave value
   LSR                  ; | Half only
   CLC                  ; | Clear Carry for addition
   ADC $22              ; | Add value to layer X position (low byte)
   STA $7F9E01,x        ; | store to HDMA table (low byte)
   LDA $23              ; | Get high byte of X position
   ADC #$00             ; | Add value to layer X position (low byte)
   STA $7F9E02,x        ;/  store to HDMA table (high byte)

   PLY                  ;\  Pull original loop counter
   CPY #$16             ; | Compare if we have written enough HDMA entries.
   BPL .End             ; | If bigger, end HDMA
   INX                  ; | Increase X, so that in the next loop, it writes the new table data...
   INX                  ; | ... at the end of the old one instead of overwritting it.
   INX                  ; | 
   INY                  ; | Increase loop counter
   BRA .Loop            ;/  Repeat loop

.End:                   ;\  Jump here when at the end of HDMA
   PLB                  ; | Pull back data bank.
   LDA #$00             ; | End HDMA by writting 00...
   STA $7F9E03,x        ; | ...at the end of the table.
   RTL                  ;/  

	RTL
	



.WaveTable:             ;\  
   db $00               ; | 
   db $02               ; | 
   db $04               ; | 
   db $06               ; | 
   db $08               ; | 
   db $0A               ; | 
   db $0C               ; | 
   db $0E               ; | 
   db $0E               ; | 
   db $0C               ; | 
   db $0A               ; | 
   db $08               ; | 
   db $06               ; | 
   db $04               ; | 
   db $02               ; | 
   db $00               ;/  



init:          ; 
	;	000abcde a = Object b = BG 4 c = BG 3 d = BG 2 e = BG 1
	  LDA #$17  ; 17
	STA $212C ; Main Screen

	;	000abcde a = Object b = BG 4 c = BG 3 d = BG 2 e = BG 1
	LDA.b #%00010011  ; 13
	STA $212D ; Sub Screen

   LDA #$11    ; | BG1, OBJ on main screen should use windowing. (TMW)
   STA $212E   ;/  
   LDA #$13    ; | BG2, BG3 on sub screen should use windowing. (TSW)
   STA $212F   ;/  

; Layer prio fix
   LDA.b #%00100100
	STA $40 ; CGADDSUB 



  REP #$20             ; | 
   LDA #$1102           ; | Use Mode 02 on register 2111
   STA $4350            ; | 4350 = Mode, 4351 = Register
   LDA #$9E00           ; | Address of HDMA table
   STA $4352            ; | 4352 = Low-Byte of table, 4353 = High-Byte of table
   SEP #$20             ; | 
   LDA.b #$7F           ; | Address of HDMA table, get bank byte
   STA $4354            ; | 4354 = Bank-Byte of table
   LDA #$20             ; | 
   TSB $0D9F|!addr      ; | Enable HDMA channel 5

RTL 





