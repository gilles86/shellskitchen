; reset_on_off.asm
; This code will reset the On/Off blocks to "Off" state (which is 00)
; every time the player enters the level.

init:
    LDA #$00        ; Load 00 into the A register (representing "Off" state)
    STA $14AF       ; Store the value of A into the On/Off block flag address
    RTL   