
init:
	LDA #$7F
	STA $78
;	000abcde a = Object b = BG 4 c = BG 3 d = BG 2 e = BG 1
	  LDA #$17  ; 17
	STA $212C ; Main Screen

	;	000abcde a = Object b = BG 4 c = BG 3 d = BG 2 e = BG 1
	LDA.b #%00010011  ; 13
	STA $212D ; Sub Screen

   LDA #$11    ; | BG1, OBJ on main screen should use windowing. (TMW)
   STA $212E   ;/  
   LDA #$13    ; | BG2, BG3 on sub screen should use windowing. (TSW)
   STA $212F   ;/  

; Layer prio fix
   LDA.b #%00100100
	STA $40 ; CGADDSUB 
;;;;;;;;;;;;;;;;;;;;

	REP #$20
	LDA #$3200
	STA $4330
	LDA #.RedTable
	STA $4332
	LDY.b #.RedTable>>16
	STY $4334
	LDA #$3200
	STA $4340
	LDA #.GreenTable
	STA $4342
	LDY.b #.GreenTable>>16
	STY $4344
	LDA #$3200
	STA $4350
	LDA #.BlueTable
	STA $4352
	LDY.b #.BlueTable>>16
	STY $4354
	SEP #$20
	LDA #$38
	TSB $0D9F|!addr
	RTL


.RedTable:           ; 
   db $03 : db $3F   ; 
   db $13 : db $3E   ; 
   db $13 : db $3D   ; 
   db $13 : db $3C   ; 
   db $12 : db $3B   ; 
   db $13 : db $3A   ; 
   db $13 : db $39   ; 
   db $13 : db $38   ; 
   db $12 : db $37   ; 
   db $18 : db $36   ; 
   db $06 : db $37   ; 
   db $06 : db $38   ; 
   db $06 : db $39   ; 
   db $06 : db $3A   ; 
   db $06 : db $3B   ; 
   db $05 : db $3C   ; 
   db $06 : db $3D   ; 
   db $06 : db $3E   ; 
   db $00            ; 

.GreenTable:         ; 
   db $10 : db $4C   ; 
   db $17 : db $4B   ; 
   db $17 : db $4A   ; 
   db $18 : db $49   ; 
   db $17 : db $48   ; 
   db $17 : db $47   ; 
   db $18 : db $46   ; 
   db $1F : db $45   ; 
   db $17 : db $46   ; 
   db $0E : db $47   ; 
   db $00            ; 

.BlueTable:          ; 
   db $3F : db $84   ; 
   db $63 : db $83   ; 
   db $3E : db $82   ; 
   db $00            ; 


main:
	LDA #$7F
	STA $78
    RTL 