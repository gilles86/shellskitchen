incsrc "asm/work/library.asm"
incsrc "../../other/macro_library.asm"
!level_nmi	= 0
!overworld_nmi	= 0
!gamemode_nmi	= 1
!global_nmi	= 0

!sprite_RAM	= $7FAC80

autoclean $949269
autoclean $97843B
autoclean $97F009
autoclean $978460
autoclean $9784C4
autoclean $9784E8
autoclean $978507
autoclean $97854B
autoclean $95FFEF
autoclean $9783A1
autoclean $96FFC4
autoclean $97EF6F
autoclean $96FFD7
autoclean $96FFEE
autoclean $97844B
autoclean $9784B6
autoclean $97D26D
autoclean $9784DD
autoclean $9784F5
autoclean $978502
autoclean $97851A
autoclean $9785F7
autoclean $978604
autoclean $978618
autoclean $9785E1
autoclean $978629
autoclean $97D6E8
autoclean $97864C
autoclean $97866B
autoclean $9786C0
autoclean $97CF82
autoclean $97F872
autoclean $978628
autoclean $97D6E4
autoclean $978688
autoclean $978735
autoclean $97D041
autoclean $97F68D
autoclean $978646
autoclean $9786A0
autoclean $97F67F
autoclean $978228
autoclean $96FE4B
autoclean $978000
autoclean $96FD6D
autoclean $97E40A
autoclean $96FC72
autoclean $96FBE9
autoclean $96FAB7
autoclean $96F7B2
autoclean $96F732
autoclean $96F6D7
autoclean $96F6B6
autoclean $96F625
autoclean $96F290
autoclean $97BB56
autoclean $96EF92
autoclean $96EF7D
autoclean $96EF08
autoclean $96EE13
autoclean $96EC8F
autoclean $96EAEB
autoclean $96EA32
autoclean $96E9D5
autoclean $96E9AE
autoclean $96E98F
autoclean $96E8D2
autoclean $96E86E
autoclean $93FA1C
autoclean $96E703
autoclean $96E645
autoclean $96E615
autoclean $96E5D2
autoclean $96E55D
autoclean $96CFA6
autoclean $94BC18
autoclean $96E50A

!previous_mode = !sprite_RAM+(!sprite_slots*3)

incsrc level.asm
incsrc overworld.asm
incsrc gamemode.asm
incsrc global.asm
incsrc sprites.asm
incsrc statusbar.asm


print freespaceuse
