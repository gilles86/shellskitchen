!GreenHeadTile = $8A
!GreenBodyTile = $E8
!GreenProp = $0B
!YellowHeadTile = $8A
!YellowBodyTile = $E8
!YellowProp = $05

Green:	db !GreenHeadTile,!GreenProp
		db !GreenBodyTile,!GreenProp
Yellow:	db !YellowHeadTile,!YellowProp
		db !YellowBodyTile,!YellowProp

Print "MAIN ",pc
PHB : PHK : PLB
JSR START_SPRITE_CODE
PLB : RTL

START_SPRITE_CODE:
LDA !minor_extended_timer,x
ASL : TAY
LDA Green,y : STA $08
LDA Green+1,y
BCS $02
EOR #$40 : STA $09
%MinorExtendedGetDrawInfo_Fix()
BCC No_Write
LDA $00 : STA $0200|!Base2,y
LDA $01 : STA $0201|!Base2,y
LDA $08 : STA $0202|!Base2,y
LDA $09 : ORA $64 : STA $0203|!Base2,y
TYA : LSR #2 : TAY
LDA #$02 : ORA $02 : STA $0420|!Base2,y
No_Write:
LDA !minor_extended_table_1,x : BEQ ScoreSpawned
LDY !minor_extended_num,x : BEQ ScreenOutScore
DEC !minor_extended_table_1,x : BNE Return
ScreenOutScore:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TXY
LDX #$05
-
LDA $16E1|!Base2,x : BEQ ++
DEX : BPL -
DEC $18F7|!Base2 : BPL +
LDX #$05 : STX $18F7|!Base2
BRA ++
+
LDX $18F7|!Base2
++
LDA !minor_extended_table_2,y
CLC : ADC #$05 : STA $16E1|!Base2,x
LDA !minor_extended_y_low,y
SEC : SBC #$08
STA $16E7|!Base2,x
XBA
LDA !minor_extended_y_high,y
SBC #$00 : STA $16F9|!Base2,x
XBA
SEC : SBC $1C
CMP #$F0 : BCC +
LDA $16E7|!Base2,x : ADC #$10 : STA $16E7|!Base2,x
BCC $03 : INC $16F9|!Base2,x
+
LDA !minor_extended_x_low,y : STA $16ED|!Base2,x
LDA !minor_extended_x_high,y : STA $16F3|!Base2,x
LDA #$30 : STA $16FF|!Base2,x
TYX
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ScoreSpawned:
LDA $9D : BNE Return
%SpeedX()
%SpeedY()
LDA !minor_extended_y_speed,x
CMP #$40 : BPL Return
INC #2
STA !minor_extended_y_speed,x
Return:
RTS

