﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace MarioStartEditor {

    public partial class MainEditor : Form {

        private byte[] bytes0F;
        private byte[] bytes10;

        private int currPal = 2;

        private string currDir = Directory.GetCurrentDirectory();

        private Bitmap overview = null;
        private Bitmap preview = null;
        private Bitmap startScreen_top = new Bitmap(255,16);
        private Bitmap startScreen_btm = new Bitmap(255,16);

        private Bitmap paletteView;
        private int renderSize;
        private Color selectColor = Color.White;

        private TileData previewData = null;
        private TileData[] upperRowStrtScreen = new TileData[20];
        private TileData[] lowerRowStrtScreen = new TileData[20];
        private TileData[] upperRowGmOver = new TileData[9];
        private TileData[] lowerRowGmOver = new TileData[9];
        private TileData[] upperRowTimeUp = new TileData[9];
        private TileData[] lowerRowTimeUp = new TileData[9];
        private TileData[] upperRowBonus = new TileData[11];
        private TileData[] lowerRowBonus = new TileData[11];

        private bool renderSpclRegions;
        private bool renderStartGrid;
        private bool renderStartBox = true;
        private bool renderTileGrid;

        private Color spclRegions = Color.Red;
        private Color tGridColor = Color.Yellow;
        private Color screenColor = Color.Yellow;

        private static Color[] pal1 = new Color[16];
        private static Color[] pal2 = new Color[16];
        private static Color[] pal3 = {
                                        Color.FromArgb(0,0,0),
                                        Color.FromArgb(0xF8,0xF8,0xF8),
                                        Color.FromArgb(0,0,0),
                                        Color.FromArgb(0xF8,0x78,0),
                                        Color.FromArgb(0xF8,0xC0,0),
                                        Color.FromArgb(0xF8,0xF8,0),
                                        Color.FromArgb(0xB8,0x28,0),
                                        Color.FromArgb(0xF8,0x88,0),
                                        Color.FromArgb(0,0,0),
                                        Color.FromArgb(0xF8,0xF8,0xF8),
                                        Color.FromArgb(0,0,0),
                                        Color.FromArgb(0,0xC8,0),
                                        Color.FromArgb(0xE8,0x18,0x68),
                                        Color.FromArgb(0xF0,0x40,0xA8),
                                        Color.FromArgb(0xF8,0x78,0xC8),
                                        Color.FromArgb(0xF8,0xC8,0xF0)}; // Standard sprite palette row 2
        private static Color[] pal4 = new Color[16];
        private static Color[] pal5 = new Color[16];
        private static Color[] pal6 = new Color[16];
        private static Color[] pal7 = new Color[16];
        private static Color[] pal8 = new Color[16];

        Color[][] palettes = { pal1, pal2, pal3, pal4, pal5, pal6, pal7, pal8 };

        private int[] position = { 
                                   00,01,02,03,04,05,
                                   16,17,18,19,20,21,
                                   74,75,76,77,78,79,
                                   90,91,92,93,94,95
                               };
        private int[] bonusPosition = {
                                   00,00,
                                   00,01,02,03,04,05,
                                   16,17,18,19,20,21,
                                   74,75,76,77,78,79,
                                   90,91,92,93,94,95
                               };

        int[] indexOffset = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };

        public MainEditor() {
            InitializeComponent();
            if (File.Exists(currDir + "/mariostart.mstrt")) {
                if (!readFromFile(File.ReadAllBytes(currDir + "/mariostart.mstrt"))) { fillEmpty(); }
            } else { fillEmpty(); }
            if (File.Exists(currDir + "/palette.pal")) {
                byte[] file = File.ReadAllBytes(currDir + "/palette.pal");
                this.updatePalettes(file);
                this.render(16);
                this.updatePalette(0);
                this.gb_palette.Enabled = true;
                repaintCurrentScreen();
            }
            if (File.Exists(currDir + "/GFX0F.bin")) {
                this.bytes0F = File.ReadAllBytes(currDir + "/GFX0F.bin");
                renderOverview(true, currPal);
                repaintCurrentScreen();
            }
            if (File.Exists(currDir + "/GFX10.bin")) {
                this.bytes10 = File.ReadAllBytes(currDir + "/GFX10.bin");
                renderOverview(false, currPal);
                repaintCurrentScreen();
            }
            drawStartGrid();
            checkUnlock();
        }

        private void fillEmpty() {
            for (int i = 0; i < 20; i++) {
                upperRowStrtScreen[i] = new TileData(0xff, 0);
                lowerRowStrtScreen[i] = new TileData(0xff, 0);
            }
            for (int i = 0; i < 9; i++) {
                upperRowGmOver[i] = new TileData(0xff, 0);
                lowerRowGmOver[i] = new TileData(0xff, 0);
                upperRowTimeUp[i] = new TileData(0xff, 0);
                lowerRowTimeUp[i] = new TileData(0xff, 0);
                upperRowBonus[i] = new TileData(0xff, 0);
                lowerRowBonus[i] = new TileData(0xff, 0);
            }
            for (int i = 9; i < 11; i++) {
                upperRowBonus[i] = new TileData(0xff, 0);
                lowerRowBonus[i] = new TileData(0xff, 0);
            }
        }

        public void updatePalette(int pal) {
            this.currPal = pal;
            renderOverview(true, pal);
            renderOverview(false, pal);
        }

        private void renderOverview(bool gfx0f, int palette, int size = 2) {
            if (((gfx0f) ? this.bytes0F : this.bytes10) == null) { return; }
            Bitmap output;

            if (this.overview == null)
                output = new Bitmap(256, 128);
            else
                output = this.overview;

            Graphics g = Graphics.FromImage(output);
            
            for (int tileC = 0; tileC < 128; tileC++) {
                int tile = tileC;
                int[] intArr = getInt((gfx0f)?this.bytes0F:this.bytes10, tileC, false, false);
                if (gfx0f) {
                    try {
                        tile = (this.rb_bonus.Checked) ? bonusPosition[tileC] : position[tileC];
                    } catch { this.overview = output; drawTileGrid(); return; }
                } else if(position.Contains(tile)) { continue; }
                int tileX = getX(tile);
                for (int x = 0; x < 8; x++)
                    for(int y = 0; y < 8; y++) {
                        Rectangle rec = new Rectangle(8 * size * tileX + size * x, 8 * size * (tile/16) + size * y, size, size);
                        g.FillRectangle(new Pen(palettes[palette][15 - intArr[y * 8 + x]]).Brush, rec);
                    }
                }
            this.overview = output;
            drawTileGrid();
        }

        private int getX(int tile) {
            int ret;
            for (int i = 0; i < 510; i += 16) {
                if (tile < i+16) {
                    ret = tile - i;
                    return ret;
                }
            }
            return -1;
        }

        private Bitmap setPixelToBmp(int x, int y, Color color, int size, int tileX, int tileY, Bitmap inputBmp) {
            for (int iX = 0; iX < size; iX++)
                for (int iY = 0; iY < size; iY++) {
                    try {
                        inputBmp.SetPixel(8 * size * tileX + size * x + iX, 8 * size * tileY + size * y + iY, color);
                    } catch { return null; }
                }
            return inputBmp;
        }

        private int[] getInt(byte[] b, int tile, bool flipX, bool flipY) {
            int[] output = new int[64];

            for (int i = 0; i < 16; i+=2) {
                byte[] byteArr = { b[32 * tile + i], b[32 * tile + 1 + i],
                                     b[32 * tile + 16 + i], b[32 * tile + 17 + i] };
                for (int y = 0; y < 8; y++) {
                    int mal = (flipY) ? (7 - i/2) * 8 + y : i * 4 + y;
                    string bits = getIndex(byteArr, (flipX)?y:7-y);
                    output[mal] = indexOffset[Convert.ToInt32(bits, 2)];
                }
            }
            return output;
        }
        
        public string getIndex(byte[] byteArr, int number) {
            string index = "";
            foreach (byte b in byteArr) {
                var bit = (b & (1 << number)) != 0;
                if (bit) index += "0"; else index += "1";
            }
            return index;
        }

        private void pct_tiles_Click(object sender, MouseEventArgs e) {
            int tile = (e.X >> 4) + ((e.Y >> 4) * 0x10);
            this.previewData = new TileData(tile, this.currPal);
            cb_flipX.Checked = false;
            cb_flipY.Checked = false;
            this.preview = renderPreview();
            pct_preview.Image = preview;

            gb_preview.Enabled = true;
            gb_StartScreenPreview.Enabled = true;
        }

        private Bitmap renderPreview() {
            Bitmap prev = new Bitmap(64, 64);

            int[] intArr;
            int tileC = this.previewData.getTile();
            int pal = this.previewData.getPalette();

            if (tileC == 0xff) {
                intArr = new int[64];
                for (int i = 0; i < 64; i++)
                    intArr[i] = 15;
            } else if (this.position.Contains(tileC)) {
                if (tileC > 79)
                    tileC -= 72;
                else if (tileC > 21)
                    tileC -= 62;
                else if (tileC > 5)
                    tileC -= 10;

                if (this.rb_bonus.Checked)
                    tileC += 2;
                intArr = getInt(this.bytes0F, tileC, this.previewData.getXflipped(), this.previewData.getYflipped());
            } else {
                intArr = getInt(this.bytes10, tileC, this.previewData.getXflipped(), this.previewData.getYflipped());
            }

            for (int pixX = 0; pixX < 8; pixX++)
                for (int pixY = 0; pixY < 8; pixY++) {
                    int mul = pixY * 8 + pixX;
                    prev = setPixelToBmp(pixX, pixY, palettes[pal][15-intArr[mul]], 8, 0, 0, prev);
                }
            return prev;
        }

        private void renderScreen(int tile, TileData data, bool bottom) {
            if (bytes0F == null || bytes10 == null) return;
            Bitmap startscreen = (bottom) ? startScreen_btm : startScreen_top;

            int[] intArr;
            int tileC = data.getTile();
            int pal = data.getPalette();
            if (tileC == 0xff) {
                intArr = new int[64];
                for (int i = 0; i < 64; i++)
                    intArr[i] = 15;
            } else if (this.position.Contains(tileC)) {
                if (tileC > 79)
                    tileC -= 72;
                else if (tileC > 21)
                    tileC -= 62;
                else if (tileC > 5)
                    tileC -= 10;

                if (this.rb_bonus.Checked)
                    tileC += 2;
                intArr = getInt(this.bytes0F, tileC, data.getXflipped(), data.getYflipped());
            } else {
                intArr = getInt(this.bytes10, tileC, data.getXflipped(), data.getYflipped());
            }

            for (int pixX = 0; pixX < 8; pixX++)
                for (int pixY = 0; pixY < 8; pixY++) {
                    int mul = pixY * 8 + pixX;
                    startscreen = setPixelToBmp(pixX, pixY, palettes[pal][15-intArr[mul]], 2, tile, 0, startscreen);
                }
            if (bottom) {
                pct_startScreen_btm.Image = startscreen;
                this.startScreen_btm = startscreen;
            } else {
                pct_startScreen_top.Image = startscreen;
                this.startScreen_top = startscreen;
            }
            this.drawStartGrid();
        }

        private void renderScreen(TileData[] data, bool bottom) {
            if (bytes0F == null || bytes10 == null) return;
            Bitmap startscreen = (bottom) ? startScreen_btm : startScreen_top;

            for (int tile = 0; tile < data.Length; tile++) {
                int[] intArr;
                int tileC = data[tile].getTile();
                int pal = data[tile].getPalette();
                if (tileC == 0xff) {
                    intArr = new int[64];
                    for (int i = 0; i < 64; i++)
                        intArr[i] = 15;
                } else if (this.position.Contains(tileC)) {
                    if (tileC > 79)
                        tileC -= 72;
                    else if (tileC > 21)
                        tileC -= 62;
                    else if (tileC > 5)
                        tileC -= 10;

                    if (this.rb_bonus.Checked)
                        tileC += 2;
                    intArr = getInt(this.bytes0F, tileC, data[tile].getXflipped(), data[tile].getYflipped());
                } else {
                    intArr = getInt(this.bytes10, tileC, data[tile].getXflipped(), data[tile].getYflipped());
                }

                for (int pixX = 0; pixX < 8; pixX++)
                    for (int pixY = 0; pixY < 8; pixY++) {
                        int mul = pixY * 8 + pixX;
                        startscreen = setPixelToBmp(pixX, pixY, palettes[pal][15 - intArr[mul]], 2, tile, 0, startscreen);
                    }
            }
            if (bottom) {
                pct_startScreen_btm.Image = startscreen;
                this.startScreen_btm = startscreen;
            } else {
                pct_startScreen_top.Image = startscreen;
                this.startScreen_top = startscreen;
            }
            this.drawStartGrid();
        }

        private void pct_startScreen_top_Click(object sender, MouseEventArgs e) {
            if (this.previewData == null && e.Button == MouseButtons.Right) return;
            int tile = (e.X >> 4);

            if (e.Button == MouseButtons.Left) {
                if (rb_startscreen.Checked) {
                    if (rb_mario.Checked)
                        upperRowStrtScreen[13 - tile] = previewData.Clone();
                    else if (tile < 6)
                        upperRowStrtScreen[19 - tile] = previewData.Clone();
                    else
                        upperRowStrtScreen[7 - (tile - 6)] = previewData.Clone();
                } else if (rb_gameover.Checked)
                    upperRowGmOver[8 - tile] = previewData.Clone();
                else if (rb_timeup.Checked)
                    upperRowTimeUp[8 - tile] = previewData.Clone();
                else if (rb_bonus.Checked)
                    upperRowBonus[10 - tile] = previewData.Clone();
                renderScreen(tile, this.previewData, false);
            } else if (e.Button == MouseButtons.Right) {
                if (rb_startscreen.Checked) {
                    if (rb_mario.Checked)
                        previewData = upperRowStrtScreen[13 - tile].Clone();
                    else if (tile < 6)
                        previewData = upperRowStrtScreen[19 - tile].Clone();
                    else
                        previewData = upperRowStrtScreen[7 - (tile - 6)].Clone();
                } else if (rb_gameover.Checked) {
                    previewData = upperRowGmOver[8 - tile].Clone();
                } else if (rb_timeup.Checked) {
                    previewData = upperRowTimeUp[8 - tile].Clone();
                } else if (rb_bonus.Checked) {
                    previewData = upperRowBonus[10 - tile].Clone();
                }
                this.preview = renderPreview();
                pct_preview.Image = this.preview;
                cb_flipX.Checked = previewData.getXflipped();
                cb_flipY.Checked = previewData.getYflipped();
            }
        }

        private void pct_startScreen_btm_Click(object sender, MouseEventArgs e) {
            if (this.previewData == null && e.Button == MouseButtons.Right) return;
            int tile = (e.X >> 4);

            if (e.Button == MouseButtons.Left) {
                if (rb_startscreen.Checked) {
                    if (rb_mario.Checked)
                        lowerRowStrtScreen[13 - tile] = previewData.Clone();
                    else if (tile < 6)
                        lowerRowStrtScreen[19 - tile] = previewData.Clone();
                    else
                        lowerRowStrtScreen[7 - (tile - 6)] = previewData.Clone();
                } else if (rb_gameover.Checked)
                    lowerRowGmOver[8 - tile] = previewData.Clone();
                else if (rb_timeup.Checked)
                    lowerRowTimeUp[8 - tile] = previewData.Clone();
                else if (rb_bonus.Checked)
                    lowerRowBonus[10 - tile] = previewData.Clone();
                renderScreen(tile, this.previewData, true);
            } else if(e.Button == MouseButtons.Right) {
                if (rb_startscreen.Checked) {
                    if (rb_mario.Checked)
                        previewData = lowerRowStrtScreen[13 - tile].Clone();
                    else if (tile < 6)
                        previewData = lowerRowStrtScreen[19 - tile].Clone();
                    else
                        previewData = lowerRowStrtScreen[7 - (tile - 6)].Clone();
                } else if (rb_gameover.Checked) {
                    previewData = lowerRowGmOver[8 - tile].Clone();
                } else if (rb_timeup.Checked) {
                    previewData = lowerRowTimeUp[8 - tile].Clone();
                } else if (rb_bonus.Checked) {
                    previewData = lowerRowBonus[10 - tile].Clone();
                }
                this.preview = renderPreview();
                pct_preview.Image = this.preview;
                cb_flipX.Checked = previewData.getXflipped();
                cb_flipY.Checked = previewData.getYflipped();
            }
        }

        private void btn_empty_Click(object sender, EventArgs e) {
            Bitmap prev = new Bitmap(64, 64);
            for (int x = 0; x < 8; x++)
                for (int y = 0; y < 8; y++)
                    prev = setPixelToBmp(x, y, Color.Black, 8, 0, 0, prev);
            this.previewData = new TileData(0xff, 0);
            this.preview = prev;
            pct_preview.Image = prev;
        }

        private void ChangeOverviewPalette(Color[] newPal) {
            ColorPalette pal = this.overview.Palette;
            for (int i = 0; i < 16; i++)
                pal.Entries[i] = newPal[i];
            this.overview.Palette = pal;
            pct_tiles.Image = overview;
        }

        private void tsmi_displayBorder_CheckedChanged(object sender, EventArgs e) {
            this.renderStartBox = tsmi_displayBorder.Checked;
            pct_startScreen_top.Image = this.startScreen_top;
            pct_startScreen_btm.Image = this.startScreen_btm;
            this.drawStartGrid();
        }

        private void tsmi_changeBorderColor_Click(object sender, EventArgs e) {
            ColorDialog cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
                this.screenColor = cd.Color;
            drawStartGrid();
        }

        private void tsmi_loadGFX0F_Click(object sender, EventArgs e) {
            tsmi_file.HideDropDown();
            openGFX0F();
        }

        private void tsmi_loadGFX10_Click(object sender, EventArgs e) {
            openGFX10();
        }

        private void openGFX0F() {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Bin files | *.bin";
            ofd.Title = "Open GFX0F";
            if (ofd.ShowDialog() == DialogResult.OK) {
                this.bytes0F = File.ReadAllBytes(ofd.FileName);
                renderOverview(true, currPal);
                repaintCurrentScreen();
            }
            checkUnlock();
        }

        private void openGFX10() {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Bin files | *.bin";
            ofd.Title = "Open GFX10";
            if (ofd.ShowDialog() == DialogResult.OK) {
                this.bytes10 = File.ReadAllBytes(ofd.FileName);
                renderOverview(false, currPal);
                repaintCurrentScreen();
            }
            checkUnlock();
        }

        private void repaintCurrentScreen() {
            if (rb_startscreen.Checked)
                repaintStartScreen();
            else if (rb_gameover.Checked)
                repaintGameOverScreen();
            else if (rb_timeup.Checked)
                repaintTimeUpScreen();
            else if (rb_bonus.Checked)
                repaintBonusScreen();
        }

        private void repaintStartScreen() {
            TileData[] data = new TileData[14];
            for (int i = 0; i < 14; i++)
                data[i] = upperRowStrtScreen[(rb_mario.Checked & i < 6) ? 13 - i : (i < 6) ? 19 - i : 7 - (i - 6)];
            renderScreen(data, false);
            for (int i = 0; i < 14; i++)
                data[i] = lowerRowStrtScreen[(rb_mario.Checked & i < 6) ? 13 - i : (i < 6) ? 19 - i : 7 - (i - 6)];
            renderScreen(data, true);
        }

        private void repaintGameOverScreen() {
            TileData[] data = new TileData[9];
            for (int i = 0; i < 9; i++)
                data[i] = upperRowGmOver[8 - i];
            renderScreen(data, false);
            for (int i = 0; i < 9; i++)
                data[i] = lowerRowGmOver[8 - i];
            renderScreen(data, true);
        }

        private void repaintTimeUpScreen() {
            TileData[] data = new TileData[9];
            for (int i = 0; i < 9; i++)
                data[i] = upperRowTimeUp[8 - i];
            renderScreen(data, false);
            for (int i = 0; i < 9; i++)
                data[i] = lowerRowTimeUp[8 - i];
            renderScreen(data, true);
        }

        private void repaintBonusScreen() {
            TileData[] data = new TileData[11];
            for (int i = 0; i < 11; i++)
                data[i] = upperRowBonus[10-i];
            renderScreen(data, false);
            for (int i = 0; i < 11; i++)
                data[i] = lowerRowBonus[10-i];
            renderScreen(data, true);
        }

        private void checkUnlock() {
            if (this.bytes0F != null)
                if (this.bytes0F.Length == 4096)
                    tsmi_loadGFX0F.Checked = true;
            if (this.bytes10 != null)
                if (this.bytes10.Length == 4096)
                    tsmi_loadGFX10.Checked = true;
            if (tsmi_loadGFX10.Checked && tsmi_loadGFX0F.Checked) {
                gb_tile.Enabled = true;
                tsmi_loadPalette.Enabled = true;
                tsmi_tileGrid.Enabled = true;
                tsmi_LoadImage.Enabled = true;
            }
        }
        
        private void tsmi_loadPalette_Click(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "YY-CHR Palette Files|*.pal";
            ofd.Title = "Select Palette File";
            if (ofd.ShowDialog() == DialogResult.OK) {
                byte[] file = File.ReadAllBytes(ofd.FileName);
                this.updatePalettes(file);
                this.render(16);
                this.updatePalette(0);
                this.gb_palette.Enabled = true;
                repaintCurrentScreen();
            }
        }

        private void tsmi_generate_code_Click(object sender, EventArgs e) {
            PatchSettings ps = new PatchSettings();
            if (ps.ShowDialog() != DialogResult.OK) return;
            CodeDisplay display = new CodeDisplay(this);
            display.displayCode(generateCode(ps.cb_startscreen.Checked,ps.cb_gameover.Checked,ps.cb_timeUp.Checked, ps.cb_bonus.Checked ,ps.cb_fixPatch.Checked, ps.cb_sa1.Checked));
            display.ShowDialog(this);
        }

        private string generateCode(bool startscreen, bool gameoverscreen, bool timeupscreen, bool bonusscreen, bool insPatch, bool sa1) {
            if (File.Exists(currDir + ((sa1)?"/templateSA1.asm":"/template.asm"))) {
                return getCodeFromFile(((sa1) ? "templateSA1.asm" : "template.asm"), startscreen, gameoverscreen, timeupscreen, bonusscreen, insPatch);
            } else {
                if (MessageBox.Show("Can't find " + ((sa1) ? "/templateSA1.asm" : "/template.asm") + ". Do you want to choose a different template file?", "Can't find file", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Select template file";
                    ofd.Filter = "ASM file | *.asm";
                    if (ofd.ShowDialog() == DialogResult.OK) {
                        return getCodeFromFile(ofd.FileName, startscreen, gameoverscreen, timeupscreen, bonusscreen, insPatch);
                    }
                }
            }
            return null;
        }

        private string getCodeFromFile(string file,bool startscreen, bool gameoverscreen, bool timeupscreen, bool bonusscreen, bool insPatch) {
            string[] tempCode = File.ReadAllLines(currDir + "/" + file);
            string[] tempNCode = new string[tempCode.Length];
            string code = "";
            int currLine = 0;
            int currNLine = 0;
            bool delete = false;
            foreach (string temp in tempCode)
            {
                switch (temp)
                {
                    case ";startscreen":
                        delete = !startscreen;
                        break;
                    case ";gameover":
                        delete = !gameoverscreen;
                        break;
                    case ";timeup":
                        delete = !timeupscreen;
                        break;
                    case ";bonus":
                        delete = !bonusscreen;
                        break;
                    case ";patch":
                        delete = !insPatch;
                        break;
                    case ";end":
                        delete = false;
                        break;
                    default:
                        if (!delete)
                        {
                            tempNCode[currNLine] = tempCode[currLine];
                            currNLine++;
                        }
                        break;
                }
                currLine++;
            }
            foreach (string tmp in tempNCode)
            {
                if (tmp == null) continue;
                code += tmp + "\n";
            }

            string temp1 = "";

            if (startscreen)
            {
                if (code.Contains("start_top:"))
                {
                    temp1 = "start_top:\ndb ";
                    for (int i = 0; i < 20; i++)
                        temp1 += "$" + upperRowStrtScreen[i].tileToHex() + ((i == 19) ? "" : ",");
                    code = code.Replace("start_top:", temp1);
                }
                if (code.Contains("start_btm:"))
                {
                    temp1 = "start_btm:\ndb ";
                    for (int i = 0; i < 20; i++)
                        temp1 += "$" + lowerRowStrtScreen[i].tileToHex() + ((i == 19) ? "" : ",");
                    code = code.Replace("start_btm:", temp1);
                }
                if (code.Contains("start_prop_top:"))
                {
                    temp1 = "start_prop_top:\ndb ";
                    for (int i = 0; i < 20; i++)
                        temp1 += "$" + upperRowStrtScreen[i].propertyToHex() + ((i == 19) ? "" : ",");
                    code = code.Replace("start_prop_top:", temp1);
                }
                if (code.Contains("start_prop_btm:"))
                {
                    temp1 = "start_prop_btm:\ndb ";
                    for (int i = 0; i < 20; i++)
                        temp1 += "$" + lowerRowStrtScreen[i].propertyToHex() + ((i == 19) ? "" : ",");
                    code = code.Replace("start_prop_btm:", temp1);
                }
            }
            if (gameoverscreen)
            {
                if (code.Contains("gameover_top:"))
                {
                    temp1 = "gameover_top:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + upperRowGmOver[i].tileToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("gameover_top:", temp1);
                }
                if (code.Contains("gameover_btm:"))
                {
                    temp1 = "gameover_btm:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + lowerRowGmOver[i].tileToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("gameover_btm:", temp1);
                }
                if (code.Contains("gameover_prop_top:"))
                {
                    temp1 = "gameover_prop_top:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + upperRowGmOver[i].propertyToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("gameover_prop_top:", temp1);
                }
                if (code.Contains("gameover_prop_btm:"))
                {
                    temp1 = "gameover_prop_btm:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + lowerRowGmOver[i].propertyToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("gameover_prop_btm:", temp1);
                }
            }
            if (timeupscreen)
            {
                if (code.Contains("timeup_top:"))
                {
                    temp1 = "timeup_top:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + upperRowTimeUp[i].tileToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("timeup_top:", temp1);
                }
                if (code.Contains("timeup_btm:"))
                {
                    temp1 = "timeup_btm:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + lowerRowTimeUp[i].tileToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("timeup_btm:", temp1);
                }
                if (code.Contains("timeup_prop_top:"))
                {
                    temp1 = "timeup_prop_top:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + upperRowTimeUp[i].propertyToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("timeup_prop_top:", temp1);
                }
                if (code.Contains("timeup_prop_btm:"))
                {
                    temp1 = "timeup_prop_btm:\ndb ";
                    for (int i = 0; i < 9; i++)
                        temp1 += "$" + lowerRowTimeUp[i].propertyToHex() + ((i == 8) ? "" : ",");
                    code = code.Replace("timeup_prop_btm:", temp1);
                }
            }
            if (bonusscreen)
            {
                if (code.Contains("bonusgame_top:"))
                {
                    temp1 = "bonusgame_top:\ndb ";
                    for (int i = 0; i < 11; i++)
                        temp1 += "$" + upperRowBonus[i].tileToHex() + ((i == 10) ? "" : ",");
                    code = code.Replace("bonusgame_top:", temp1);
                }
                if (code.Contains("bonusgame_btm:"))
                {
                    temp1 = "bonusgame_btm:\ndb ";
                    for (int i = 0; i < 11; i++)
                        temp1 += "$" + lowerRowBonus[i].tileToHex() + ((i == 10) ? "" : ",");
                    code = code.Replace("bonusgame_btm:", temp1);
                }
                if (code.Contains("bonusgame_prop_top:"))
                {
                    temp1 = "bonusgame_prop_top:\ndb ";
                    for (int i = 0; i < 11; i++)
                        temp1 += "$" + upperRowBonus[i].propertyToHex() + ((i == 10) ? "" : ",");
                    code = code.Replace("bonusgame_prop_top:", temp1);
                }
                if (code.Contains("bonusgame_prop_btm:"))
                {
                    temp1 = "bonusgame_prop_btm:\ndb ";
                    for (int i = 0; i < 11; i++)
                        temp1 += "$" + lowerRowBonus[i].propertyToHex() + ((i == 10) ? "" : ",");
                    code = code.Replace("bonusgame_prop_btm:", temp1);
                }
            }
            try
            {
                if (cb_Position.Checked)
                {
                    byte position = byte.Parse(tbYPosition.Text, System.Globalization.NumberStyles.HexNumber);
                    code = code.Replace("#Ypos_1", "#$" + position.ToString("x2")).Replace("#Ypos_2", "#$" + (position + 0x08).ToString("x2"));
                }
            }
            catch { }
            return code.Replace("#Ypos_1", "#$68").Replace("#Ypos_2", "#$70");
        }

        private void tsmi_patch_rom_Click(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "ROM file|*.smc";
            ofd.Title = "Select ROM to patch";
            if (ofd.ShowDialog() == DialogResult.OK) {
                byte[] file = File.ReadAllBytes(ofd.FileName);
                if (!verifyROM(file.Length & 0x7fff)) {
                    MessageBox.Show("The file size of the ROM does not indicate a valid SNES ROM", "Can't load ROM", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                writeROM(getHeader(file.Length), file, ofd.FileName);
            }
        }

        private int getHeader(int ROMlength) {
            return ROMlength % 0x8000;
        }

        private bool verifyROM(int remainder) {
            return (remainder != 0 || remainder != 0x200);
        }

        private void writeROM(int header, byte[] ROMcontent, string filePath) {
            PatchSettings ps = new PatchSettings();
            ps.cb_fixPatch.Enabled = false;
            ps.cb_sa1.Enabled = false;
            if (ps.ShowDialog() != DialogResult.OK) return;

            int upperOffset = SNEStoPC(0x0090D1, header);
            int lowerOffset = SNEStoPC(0x009105, header);
            int upperPropOffset = SNEStoPC(0x009139, header);
            int lowerPropOffset = SNEStoPC(0x00916A, header);
            if (ps.cb_startscreen.Checked) for (int i = 0; i < 20; i++) {
                ROMcontent[upperOffset + i] = (byte)upperRowStrtScreen[i].getTile();
                ROMcontent[lowerOffset + i] = (byte)lowerRowStrtScreen[i].getTile();
                ROMcontent[upperPropOffset + i] = upperRowStrtScreen[i].propertyToByte();
                ROMcontent[lowerPropOffset + i] = lowerRowStrtScreen[i].propertyToByte();
            }
            
            if (ps.cb_gameover.Checked) for (int i = 0; i < 9; i++) {
                ROMcontent[upperOffset + 20 + i] = (byte)upperRowGmOver[i].getTile();
                ROMcontent[lowerOffset + 20 + i] = (byte)lowerRowGmOver[i].getTile();
                ROMcontent[upperPropOffset + 20 + i] = upperRowGmOver[i].propertyToByte();
                ROMcontent[lowerPropOffset + 20 + i] = lowerRowGmOver[i].propertyToByte();
            }

            if (ps.cb_timeUp.Checked) for (int i = 0; i < 9; i++) {
                ROMcontent[upperOffset + 20 + 9 + i] = (byte)upperRowTimeUp[i].getTile();
                ROMcontent[lowerOffset + 20 + 9 + i] = (byte)lowerRowTimeUp[i].getTile();
                ROMcontent[upperPropOffset + 20 + 9 + i] = upperRowTimeUp[i].propertyToByte();
                ROMcontent[lowerPropOffset + 20 + 9 + i] = lowerRowTimeUp[i].propertyToByte();
            }
			
            if(cb_Position.Checked) {
                int height = byte.Parse(tbYPosition.Text, System.Globalization.NumberStyles.HexNumber);

                int offset = SNEStoPC(0x009225, header);
                ROMcontent[offset] = (byte)height;
                offset = SNEStoPC(0x00922A, header);
                ROMcontent[offset] = (byte)(height + 0x08);
            }
            try { File.WriteAllBytes(filePath, ROMcontent); }
            catch (Exception ex) { MessageBox.Show("Failed to write data: " + ex.Message,"Can't load ROM", MessageBoxButtons.OK,MessageBoxIcon.Error);}
        }

        private TileData[] readFromROM(int header, byte[] ROMcontent, bool upperRow) {
            TileData[] data = new TileData[52];
            int tileOffset = SNEStoPC((upperRow)?0x0090D1:0x009105, header);
            int propOffset = SNEStoPC((upperRow)?0x009139:0x00916A, header);
            for (int i = 0; i < 52; i++)
                data[i] = new TileData((byte)ROMcontent[tileOffset + i], (byte)ROMcontent[propOffset + i]);
            return data;
        }

        private int SNEStoPC(int addr, int header) {
            return (addr & 0x7FFF) + ((addr / 2) & 0xFF8000) + header;
        }

        private void rb_mario_CheckedChanged(object sender, EventArgs e) {
            TileData[] data = new TileData[6];
            if (rb_mario.Checked) {
                for (int i = 0; i < 6; i++)
                    data[i] = this.upperRowStrtScreen[13 - i];
                renderScreen(data, false);

                for (int i = 0; i < 6; i++)
                    data[i] = this.lowerRowStrtScreen[13 - i];
                renderScreen(data, true);
            } else {
                for (int i = 0; i < 6; i++)
                    data[i] = this.upperRowStrtScreen[19 - i];
                renderScreen(data, false);

                for (int i = 0; i < 6; i++)
                    data[i] = this.lowerRowStrtScreen[19 - i];
                renderScreen(data, true);
            }
        }

        private void cb_flipX_CheckedChanged(object sender, EventArgs e) {
            this.previewData.setXflipped(cb_flipX.Checked);
            this.preview = renderPreview();
            pct_preview.Image = this.preview;
        }

        private void cb_flipY_CheckedChanged(object sender, EventArgs e) {
            this.previewData.setYflipped(cb_flipY.Checked);
            this.preview = renderPreview();
            pct_preview.Image = this.preview;
        }

        private void tsmi_startGrid_CheckedChanged(object sender, EventArgs e) {
            this.renderStartGrid = tsmi_startGrid.Checked;
            pct_startScreen_top.Image = this.startScreen_top;
            pct_startScreen_btm.Image = this.startScreen_btm;
            this.drawStartGrid();
        }

        private void tsmi_spclRegions_CheckedChanged(object sender, EventArgs e) {
            this.renderSpclRegions = tsmi_spclRegions.Checked;
            pct_startScreen_top.Image = this.startScreen_top;
            pct_startScreen_btm.Image = this.startScreen_btm;
            this.drawStartGrid();
        }

        private void drawStartGrid() {
            Bitmap top = (Bitmap)this.startScreen_top.Clone();
            Bitmap btm = (Bitmap)this.startScreen_btm.Clone();

            Graphics gTop = Graphics.FromImage(top);
            Graphics gBtm = Graphics.FromImage(btm);

            Pen penGrid = new Pen(this.screenColor);
            Pen penSpclRegions = new Pen(this.spclRegions);

            if(this.renderStartGrid)
                if (rb_bonus.Checked)
                    for (int i = 1; i < 11; i++) {
                        Point pnt1 = new Point(i * 16, 0);
                        Point pnt2 = new Point(i * 16, 16);
                        gTop.DrawLine(penGrid, pnt1, pnt2);
                        gBtm.DrawLine(penGrid, pnt1, pnt2);
                    }
                else
                    for (int i = 1; i < 14; i++) {
                        Point pnt1 = new Point(i * 16, 0);
                        Point pnt2 = new Point(i * 16, 16);
                        gTop.DrawLine((i < ((rb_startscreen.Checked) ? 7 : 6) & this.renderSpclRegions) ? penSpclRegions : penGrid, pnt1, pnt2);
                        gBtm.DrawLine((i < ((rb_startscreen.Checked) ? 7 : 6) & this.renderSpclRegions) ? penSpclRegions : penGrid, pnt1, pnt2);
                }
            if (this.renderStartBox) {
                gBtm.DrawLine(penGrid, new Point(0, 15), new Point(14 * 16, 15));
                gBtm.DrawLine((this.renderSpclRegions && !rb_bonus.Checked) ? penSpclRegions : penGrid, new Point(0, 0), new Point(0, 16));
                if (rb_startscreen.Checked) gBtm.DrawLine(penGrid, new Point(223, 0), new Point(223, 16));
                else if (rb_bonus.Checked) gBtm.DrawLine(penGrid, new Point(175, 0), new Point(175, 16));
                else gBtm.DrawLine(penGrid, new Point(143, 0), new Point(143, 16));
                gTop.DrawLine(penGrid, new Point(0, 0), new Point(14 * 16, 0));
                gTop.DrawLine((this.renderSpclRegions && !rb_bonus.Checked) ? penSpclRegions : penGrid, new Point(0, 0), new Point(0, 16));
                if (rb_startscreen.Checked) gTop.DrawLine(penGrid, new Point(223, 0), new Point(223, 16));
                else if (rb_bonus.Checked) gTop.DrawLine(penGrid, new Point(175, 0), new Point(175, 16));
                else gTop.DrawLine(penGrid, new Point(143, 0), new Point(143, 16));
                
            }
            if (this.renderStartGrid)
                gBtm.DrawLine(penGrid, new Point(0, 0), new Point(14 * 16, 0));
            if (this.renderSpclRegions && !rb_bonus.Checked) {
                if (this.renderStartBox || !this.renderStartGrid) {
                    gTop.DrawLine(penSpclRegions, new Point(0, 0), new Point((rb_startscreen.Checked) ? 96 : 80, 0));
                    gBtm.DrawLine(penSpclRegions, new Point(0, 15), new Point((rb_startscreen.Checked) ? 96 : 80, 15));
                    
                    gTop.DrawLine(penSpclRegions, 0, 0, 0, 16);
                    gBtm.DrawLine(penSpclRegions, 0, 0, 0, 16);
                }
                if (this.renderStartGrid)
                    gBtm.DrawLine(penSpclRegions, new Point(0, 0), new Point((rb_startscreen.Checked)?96:80, 0));
                else {
                    gTop.DrawLine(penSpclRegions, new Point((rb_startscreen.Checked) ? 96 : 80, 0), new Point((rb_startscreen.Checked) ? 96 : 80, 16));
                    gBtm.DrawLine(penSpclRegions, new Point((rb_startscreen.Checked) ? 96 : 80, 0), new Point((rb_startscreen.Checked) ? 96 : 80, 16));
                }
            }
            pct_startScreen_top.Image = top;
            pct_startScreen_btm.Image = btm;
        }

        private void drawTileGrid() {
            Bitmap overview = (Bitmap)this.overview.Clone();
            if (this.renderTileGrid) {
                Graphics g = Graphics.FromImage(overview);
                Pen pen = new Pen(this.tGridColor);
                for (int i = 1; i < 16; i++)
                    g.DrawLine(pen, i * 16, 0, i * 16, 8 * 8 * 2);
                for (int i = 1; i < 8; i++)
                    g.DrawLine(pen, 0, i * 16, 256, i * 16);
            }
            pct_tiles.Image = overview;
        }

        private void tsmi_tileGrid_CheckedChanged(object sender, EventArgs e) {
            this.renderTileGrid = tsmi_tileGrid.Checked;
            drawTileGrid();
        }

        private void tsmi_FromROM_Click(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "ROM file|*.smc";
            ofd.Title = "Select ROM to load from";
            Console.WriteLine("test");
            if (ofd.ShowDialog() == DialogResult.OK) {
                byte[] file = File.ReadAllBytes(ofd.FileName);
                if (!verifyROM(file.Length & 0x7fff)) {
                    MessageBox.Show("The file size of the ROM does not indicate a valid SNES ROM", "Can't load ROM", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int header = getHeader(file.Length);
                TileData[] data = readFromROM(header, file, true);
                for (int i = 0; i < 0x14; i++)
                    this.upperRowStrtScreen[i] = data[i];
                for (int i = 0x14; i < 0x1D; i++)
                    this.upperRowGmOver[i-0x14] = data[i];
                for (int i = 0x1D; i < 0x26; i++)
                    this.upperRowTimeUp[i-0x1D] = data[i];
                for (int i = 0x26; i < 0x31; i++)
                    this.upperRowBonus[i - 0x26] = data[i];

                data = readFromROM(header, file, false);
                for (int i = 0; i < 0x14; i++)
                    this.lowerRowStrtScreen[i] = data[i];
                for (int i = 0x14; i < 0x1D; i++)
                    this.lowerRowGmOver[i-0x14] = data[i];
                for (int i = 0x1D; i < 0x26; i++)
                    this.lowerRowTimeUp[i - 0x1D] = data[i];
                for (int i = 0x26; i < 0x31; i++)
                    this.lowerRowBonus[i - 0x26] = data[i];

                repaintCurrentScreen();
            }
        }

        private void rb_startscreen_changed(object sender, EventArgs e) {
            rb_luigi.Enabled = rb_startscreen.Checked;
            rb_mario.Enabled = rb_startscreen.Checked;
            tsmi_spclRegions.Text = (rb_startscreen.Checked) ? "Highlight Mario/Luigi Tiles" : "Highlight tiles coming from left";
            if (rb_startscreen.Checked) {
                repaintStartScreen();
                pct_startScreen_top.Left = 144;
                pct_startScreen_btm.Left = 144;
                pct_startScreen_top.Size = new Size(224, 16);
                pct_startScreen_btm.Size = new Size(224, 16);
            }
            drawStartGrid();
        }

        private void rb_timeup_changed(object sender, EventArgs e) {
            if (rb_timeup.Checked) {
                repaintTimeUpScreen();
                pct_startScreen_top.Left = 184;
                pct_startScreen_btm.Left = 184;
                pct_startScreen_top.Size = new Size(144, 16);
                pct_startScreen_btm.Size = new Size(144, 16);
            }
        }

        private void rb_gameover_changed(object sender, EventArgs e) {
            if (rb_gameover.Checked) {
                repaintGameOverScreen();
                pct_startScreen_top.Left = 184;
                pct_startScreen_btm.Left = 184;
                pct_startScreen_top.Size = new Size(144, 16);
                pct_startScreen_btm.Size = new Size(144, 16);
            }
        }

        private void rb_Bonus_changed(object sender, EventArgs e) {
            if (rb_bonus.Checked) {
                repaintBonusScreen();
                pct_startScreen_top.Left = 168;
                pct_startScreen_btm.Left = 168;
                pct_startScreen_top.Size = new Size(176, 16);
                pct_startScreen_btm.Size = new Size(176, 16);
            }
            renderOverview(true, this.currPal);
        }

        private void tbYPosition_changed(object sender, EventArgs e) {
            try {
                int i = int.Parse(tbYPosition.Text, System.Globalization.NumberStyles.HexNumber);
                pct_startScreen_top.Top = i * 2 + 16;
                pct_startScreen_btm.Top = pct_startScreen_top.Top + 0x10;
                tbYPosition.ForeColor = Color.Black;
            } catch {
                tbYPosition.ForeColor = Color.Red;
            }
        }

        private void cbPosition_changed(object sender, EventArgs e) {
            tbYPosition.Enabled = cb_Position.Checked;
            if (!cb_Position.Checked) tbYPosition.Text = "68";
        }

        private void tsmi_FromEditFile_Click(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Mario Editor Files | *.mstrt";
            ofd.Title = "Select Mario Editor File";
            if (ofd.ShowDialog() != DialogResult.OK) return;
            byte[] file = File.ReadAllBytes(ofd.FileName);
            readFromFile(file);
            repaintCurrentScreen();
        }

        private bool readFromFile(byte[] file) {
            if (!(file[0] == 0x11 && file[1] == 0xDD && file[2] == 0xA9 && file[3] == 0xC5 && file.Length == 202)) { MessageBox.Show("The file you tried to load isn't in the correct format. Make sure the file was made in the same version as the tool.", "Can't load file", MessageBoxButtons.OK, MessageBoxIcon.Error); return false; }
            int header = 4;
            for (int i = 0; i < 0x14 * 2; i += 2) {
                upperRowStrtScreen[i / 2] = new TileData(file[header + i], file[header + i + 1]);
                lowerRowStrtScreen[i / 2] = new TileData(file[header + 40 + i], file[header + 41 + i]);
            }
            for (int i = 0; i < 9 * 2; i += 2) {
                upperRowGmOver[i / 2] = new TileData(file[header + 80 + i], file[header + 81 + i]);
                lowerRowGmOver[i / 2] = new TileData(file[header + 98 + i], file[header + 99 + i]);

                upperRowTimeUp[i / 2] = new TileData(file[header + 116 + i], file[header + 117 + i]);
                lowerRowTimeUp[i / 2] = new TileData(file[header + 134 + i], file[header + 135 + i]);
            }
            for (int i = 0; i < 11 * 2; i += 2) {
                upperRowBonus[i / 2] = new TileData(file[header + 152 + i], file[header + 153 + i]);
                lowerRowBonus[i / 2] = new TileData(file[header + 175 + i], file[header + 176 + i]);
            }
            return true;
        }

        private void tsmi_createFile_Click(object sender, EventArgs e) {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Mario Editor Files | *.mstrt";
            if (sfd.ShowDialog() != DialogResult.OK) return;
            byte[] file = new byte[202];
            byte header = 4;
            file[0] = 0x11;
            file[1] = 0xDD;
            file[2] = 0xA9;
            file[3] = 0xC5;
            for (int i = 0; i < 0x14 * 2; i += 2) {
                file[header + i] = (byte)upperRowStrtScreen[i / 2].getTile();
                file[header + i + 1] = upperRowStrtScreen[i / 2].propertyToByte();

                file[header + 40 + i] = (byte)lowerRowStrtScreen[i / 2].getTile();
                file[header + 41 + i] = lowerRowStrtScreen[i / 2].propertyToByte();
            }
            for (int i = 0; i < 9 * 2; i += 2) {
                file[header + 80 + i] = (byte)upperRowGmOver[i / 2].getTile();
                file[header + 81 + i] = upperRowGmOver[i / 2].propertyToByte();

                file[header + 98 + i] = (byte)lowerRowGmOver[i / 2].getTile();
                file[header + 99 + i] = lowerRowGmOver[i / 2].propertyToByte();

                file[header + 116 + i] = (byte)upperRowTimeUp[i / 2].getTile();
                file[header + 117 + i] = upperRowTimeUp[i / 2].propertyToByte();

                file[header + 134 + i] = (byte)lowerRowTimeUp[i / 2].getTile();
                file[header + 135 + i] = lowerRowTimeUp[i / 2].propertyToByte();
            }
            for (int i = 0; i < 11 * 2; i += 2) {
                file[header + 152 + i] = (byte)upperRowBonus[i / 2].getTile();
                file[header + 153 + i] = upperRowBonus[i / 2].propertyToByte();

                file[header + 175 + i] = (byte)lowerRowBonus[i / 2].getTile();
                file[header + 176 + i] = lowerRowBonus[i / 2].propertyToByte();
            }
            Stream fileStream = sfd.OpenFile();
            fileStream.Seek(0, SeekOrigin.Begin);
            fileStream.Write(file, 0, file.Length);
            fileStream.Close();
        }

        public void updatePalettes(byte[] newPal) {
            for (int pal = 8; pal < 16; pal++)
                for (int col = 0; col < 16 * 3; col += 3) {
                    int r = 3 * 16 * pal + col; int g = r + 1; int b = r + 2;
                    palettes[pal - 8][col / 3] = Color.FromArgb(newPal[r], newPal[g], newPal[b]);
                }
        }

        public void render(int size) {
            Bitmap bmp = new Bitmap(size * 16, size * 8);
            for (int pal = 0; pal < 8; pal++)
                for (int col = 0; col < 16; col++) {
                    for (int x = 0; x < size; x++)
                        for (int y = 0; y < size; y++)
                            bmp.SetPixel(size * col + x, size * pal + y, palettes[pal][col]);
                }
            this.renderSize = size;
            this.paletteView = bmp;
            this.drawSelection(0);
        }

        private void pct_paletteView_Click(object sender, MouseEventArgs e) {
            int palette = (e.Y >> 4);
            this.updatePalette(palette);
            this.drawSelection(palette);
        }

        private void drawSelection(int palette) {
            Bitmap bmp = (Bitmap)this.paletteView.Clone();
            Graphics g = Graphics.FromImage(bmp);
            g.DrawRectangle(new Pen(this.selectColor), new Rectangle(new Point(0, palette * this.renderSize),
                new Size(16 * this.renderSize - 1, this.renderSize)));
            pct_paletteView.Image = bmp;
        }
    }
}