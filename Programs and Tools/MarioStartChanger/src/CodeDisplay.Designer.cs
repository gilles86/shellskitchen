﻿namespace MarioStartEditor
{
    partial class CodeDisplay
    {
		/// Automatically generated code
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Automatically generated code

        private void InitializeComponent()
        {
            this.rtb_codeDisplay = new System.Windows.Forms.RichTextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtb_codeDisplay
            // 
            this.rtb_codeDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb_codeDisplay.Location = new System.Drawing.Point(0, 0);
            this.rtb_codeDisplay.Name = "rtb_codeDisplay";
            this.rtb_codeDisplay.ReadOnly = true;
            this.rtb_codeDisplay.Size = new System.Drawing.Size(284, 266);
            this.rtb_codeDisplay.TabIndex = 0;
            this.rtb_codeDisplay.Text = "";
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save.BackColor = System.Drawing.SystemColors.Control;
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_save.Location = new System.Drawing.Point(178, 12);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 1;
            this.btn_save.Text = "Save File";
            this.btn_save.UseVisualStyleBackColor = false;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // CodeDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 266);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.rtb_codeDisplay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "CodeDisplay";
            this.Text = "ASM Code";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_codeDisplay;
        private System.Windows.Forms.Button btn_save;

    }
}