﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MarioStartEditor {
    public partial class PatchSettings : Form {
        public PatchSettings() {
            InitializeComponent();
        }

        private void btn_apply_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
