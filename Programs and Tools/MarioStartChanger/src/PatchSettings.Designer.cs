﻿namespace MarioStartEditor
{
    partial class PatchSettings
    {
        /// Automatically generated code
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Automatically generated code

        private void InitializeComponent()
        {
            this.btn_apply = new System.Windows.Forms.Button();
            this.gb_Screens = new System.Windows.Forms.GroupBox();
            this.cb_timeUp = new System.Windows.Forms.CheckBox();
            this.cb_gameover = new System.Windows.Forms.CheckBox();
            this.cb_startscreen = new System.Windows.Forms.CheckBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.gb_patch = new System.Windows.Forms.GroupBox();
            this.cb_fixPatch = new System.Windows.Forms.CheckBox();
            this.cb_sa1 = new System.Windows.Forms.CheckBox();
            this.gb_other = new System.Windows.Forms.GroupBox();
            this.cb_bonus = new System.Windows.Forms.CheckBox();
            this.gb_Screens.SuspendLayout();
            this.gb_patch.SuspendLayout();
            this.gb_other.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_apply
            // 
            this.btn_apply.Location = new System.Drawing.Point(2, 199);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(60, 23);
            this.btn_apply.TabIndex = 0;
            this.btn_apply.Text = "OK";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // gb_Screens
            // 
            this.gb_Screens.Controls.Add(this.cb_bonus);
            this.gb_Screens.Controls.Add(this.cb_timeUp);
            this.gb_Screens.Controls.Add(this.cb_gameover);
            this.gb_Screens.Controls.Add(this.cb_startscreen);
            this.gb_Screens.Location = new System.Drawing.Point(2, 3);
            this.gb_Screens.Name = "gb_Screens";
            this.gb_Screens.Size = new System.Drawing.Size(139, 107);
            this.gb_Screens.TabIndex = 1;
            this.gb_Screens.TabStop = false;
            this.gb_Screens.Text = "Select Screens To Insert";
            // 
            // cb_timeUp
            // 
            this.cb_timeUp.AutoSize = true;
            this.cb_timeUp.Checked = true;
            this.cb_timeUp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_timeUp.Location = new System.Drawing.Point(6, 61);
            this.cb_timeUp.Name = "cb_timeUp";
            this.cb_timeUp.Size = new System.Drawing.Size(103, 17);
            this.cb_timeUp.TabIndex = 2;
            this.cb_timeUp.Text = "Time Up Screen";
            this.cb_timeUp.UseVisualStyleBackColor = true;
            // 
            // cb_gameover
            // 
            this.cb_gameover.AutoSize = true;
            this.cb_gameover.Checked = true;
            this.cb_gameover.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_gameover.Location = new System.Drawing.Point(6, 39);
            this.cb_gameover.Name = "cb_gameover";
            this.cb_gameover.Size = new System.Drawing.Size(117, 17);
            this.cb_gameover.TabIndex = 1;
            this.cb_gameover.Text = "Game Over Screen";
            this.cb_gameover.UseVisualStyleBackColor = true;
            // 
            // cb_startscreen
            // 
            this.cb_startscreen.AutoSize = true;
            this.cb_startscreen.Checked = true;
            this.cb_startscreen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_startscreen.Location = new System.Drawing.Point(6, 19);
            this.cb_startscreen.Name = "cb_startscreen";
            this.cb_startscreen.Size = new System.Drawing.Size(85, 17);
            this.cb_startscreen.TabIndex = 0;
            this.cb_startscreen.Text = "Start Screen";
            this.cb_startscreen.UseVisualStyleBackColor = true;
            // 
            // btn_cancel
            // 
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(81, 199);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(60, 23);
            this.btn_cancel.TabIndex = 3;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // gb_patch
            // 
            this.gb_patch.Controls.Add(this.cb_fixPatch);
            this.gb_patch.Location = new System.Drawing.Point(2, 112);
            this.gb_patch.Name = "gb_patch";
            this.gb_patch.Size = new System.Drawing.Size(139, 39);
            this.gb_patch.TabIndex = 4;
            this.gb_patch.TabStop = false;
            this.gb_patch.Text = "Patch";
            // 
            // cb_fixPatch
            // 
            this.cb_fixPatch.AutoSize = true;
            this.cb_fixPatch.Location = new System.Drawing.Point(6, 15);
            this.cb_fixPatch.Name = "cb_fixPatch";
            this.cb_fixPatch.Size = new System.Drawing.Size(109, 17);
            this.cb_fixPatch.TabIndex = 0;
            this.cb_fixPatch.Text = "Install Row Patch";
            this.cb_fixPatch.UseVisualStyleBackColor = true;
            // 
            // cb_sa1
            // 
            this.cb_sa1.AutoSize = true;
            this.cb_sa1.Location = new System.Drawing.Point(6, 18);
            this.cb_sa1.Name = "cb_sa1";
            this.cb_sa1.Size = new System.Drawing.Size(109, 17);
            this.cb_sa1.TabIndex = 1;
            this.cb_sa1.Text = "Use SA-1 Version";
            this.cb_sa1.UseVisualStyleBackColor = true;
            // 
            // gb_other
            // 
            this.gb_other.Controls.Add(this.cb_sa1);
            this.gb_other.Location = new System.Drawing.Point(2, 157);
            this.gb_other.Name = "gb_other";
            this.gb_other.Size = new System.Drawing.Size(139, 38);
            this.gb_other.TabIndex = 5;
            this.gb_other.TabStop = false;
            this.gb_other.Text = "Other Options";
            // 
            // cb_bonus
            // 
            this.cb_bonus.AutoSize = true;
            this.cb_bonus.Checked = true;
            this.cb_bonus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_bonus.Location = new System.Drawing.Point(6, 82);
            this.cb_bonus.Name = "cb_bonus";
            this.cb_bonus.Size = new System.Drawing.Size(124, 17);
            this.cb_bonus.TabIndex = 3;
            this.cb_bonus.Text = "Bonus Game Screen";
            this.cb_bonus.UseVisualStyleBackColor = true;
            // 
            // PatchSettings
            // 
            this.AcceptButton = this.btn_apply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(145, 234);
            this.Controls.Add(this.gb_other);
            this.Controls.Add(this.gb_patch);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.gb_Screens);
            this.Controls.Add(this.btn_apply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatchSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Patch Settings";
            this.gb_Screens.ResumeLayout(false);
            this.gb_Screens.PerformLayout();
            this.gb_patch.ResumeLayout(false);
            this.gb_patch.PerformLayout();
            this.gb_other.ResumeLayout(false);
            this.gb_other.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.GroupBox gb_Screens;
        private System.Windows.Forms.Button btn_cancel;
        public System.Windows.Forms.CheckBox cb_startscreen;
        public System.Windows.Forms.CheckBox cb_gameover;
        public System.Windows.Forms.CheckBox cb_timeUp;
        private System.Windows.Forms.GroupBox gb_patch;
        public System.Windows.Forms.CheckBox cb_fixPatch;
        private System.Windows.Forms.GroupBox gb_other;
        public System.Windows.Forms.CheckBox cb_sa1;
        public System.Windows.Forms.CheckBox cb_bonus;
    }
}