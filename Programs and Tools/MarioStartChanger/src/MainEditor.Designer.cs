﻿namespace MarioStartEditor
{
    partial class MainEditor
    {
        /// Automatically generated code
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Automatically generated code
		
        private void InitializeComponent()
        {
            this.pct_tiles = new System.Windows.Forms.PictureBox();
            this.gb_tile = new System.Windows.Forms.GroupBox();
            this.btn_empty = new System.Windows.Forms.Button();
            this.gb_preview = new System.Windows.Forms.GroupBox();
            this.cb_flipY = new System.Windows.Forms.CheckBox();
            this.cb_flipX = new System.Windows.Forms.CheckBox();
            this.pct_preview = new System.Windows.Forms.PictureBox();
            this.gb_StartScreenPreview = new System.Windows.Forms.GroupBox();
            this.lbl_rightclick = new System.Windows.Forms.Label();
            this.lbl_leftclick = new System.Windows.Forms.Label();
            this.pct_startScreen_btm = new System.Windows.Forms.PictureBox();
            this.pct_startScreen_top = new System.Windows.Forms.PictureBox();
            this.rb_luigi = new System.Windows.Forms.RadioButton();
            this.rb_mario = new System.Windows.Forms.RadioButton();
            this.pct_background = new System.Windows.Forms.PictureBox();
            this.ms_main = new System.Windows.Forms.MenuStrip();
            this.tsmi_file = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_loadGFX = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_loadGFX0F = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_loadGFX10 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_loadPalette = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_LoadImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_FromROM = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_FromEditFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_patch = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_generate_code = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_patch_rom = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_createFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_display = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_startScreen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_displayBorder = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_startGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_spclRegions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_tileGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rb_startscreen = new System.Windows.Forms.RadioButton();
            this.rb_timeup = new System.Windows.Forms.RadioButton();
            this.rb_gameover = new System.Windows.Forms.RadioButton();
            this.gb_palette = new System.Windows.Forms.GroupBox();
            this.pct_paletteView = new System.Windows.Forms.PictureBox();
            this.gbPosition = new System.Windows.Forms.GroupBox();
            this.cb_Position = new System.Windows.Forms.CheckBox();
            this.lblPosText = new System.Windows.Forms.Label();
            this.tbYPosition = new System.Windows.Forms.TextBox();
            this.panel_selectScreen = new System.Windows.Forms.Panel();
            this.rb_bonus = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pct_tiles)).BeginInit();
            this.gb_tile.SuspendLayout();
            this.gb_preview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pct_preview)).BeginInit();
            this.gb_StartScreenPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pct_startScreen_btm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_startScreen_top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_background)).BeginInit();
            this.ms_main.SuspendLayout();
            this.gb_palette.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pct_paletteView)).BeginInit();
            this.gbPosition.SuspendLayout();
            this.panel_selectScreen.SuspendLayout();
            this.SuspendLayout();
            // 
            // pct_tiles
            // 
            this.pct_tiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pct_tiles.Location = new System.Drawing.Point(6, 19);
            this.pct_tiles.Name = "pct_tiles";
            this.pct_tiles.Size = new System.Drawing.Size(256, 128);
            this.pct_tiles.TabIndex = 1;
            this.pct_tiles.TabStop = false;
            this.pct_tiles.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pct_tiles_Click);
            // 
            // gb_tile
            // 
            this.gb_tile.Controls.Add(this.btn_empty);
            this.gb_tile.Controls.Add(this.pct_tiles);
            this.gb_tile.Enabled = false;
            this.gb_tile.Location = new System.Drawing.Point(12, 208);
            this.gb_tile.Name = "gb_tile";
            this.gb_tile.Size = new System.Drawing.Size(273, 185);
            this.gb_tile.TabIndex = 2;
            this.gb_tile.TabStop = false;
            this.gb_tile.Text = "Tiles";
            // 
            // btn_empty
            // 
            this.btn_empty.Location = new System.Drawing.Point(105, 153);
            this.btn_empty.Name = "btn_empty";
            this.btn_empty.Size = new System.Drawing.Size(48, 23);
            this.btn_empty.TabIndex = 3;
            this.btn_empty.Text = "Empty";
            this.btn_empty.UseVisualStyleBackColor = true;
            this.btn_empty.Click += new System.EventHandler(this.btn_empty_Click);
            // 
            // gb_preview
            // 
            this.gb_preview.Controls.Add(this.cb_flipY);
            this.gb_preview.Controls.Add(this.cb_flipX);
            this.gb_preview.Controls.Add(this.pct_preview);
            this.gb_preview.Enabled = false;
            this.gb_preview.Location = new System.Drawing.Point(12, 406);
            this.gb_preview.Name = "gb_preview";
            this.gb_preview.Size = new System.Drawing.Size(140, 85);
            this.gb_preview.TabIndex = 3;
            this.gb_preview.TabStop = false;
            this.gb_preview.Text = "Preview";
            // 
            // cb_flipY
            // 
            this.cb_flipY.AutoSize = true;
            this.cb_flipY.Location = new System.Drawing.Point(76, 56);
            this.cb_flipY.Name = "cb_flipY";
            this.cb_flipY.Size = new System.Drawing.Size(52, 17);
            this.cb_flipY.TabIndex = 4;
            this.cb_flipY.Text = "Flip Y";
            this.cb_flipY.UseVisualStyleBackColor = true;
            this.cb_flipY.CheckedChanged += new System.EventHandler(this.cb_flipY_CheckedChanged);
            // 
            // cb_flipX
            // 
            this.cb_flipX.AutoSize = true;
            this.cb_flipX.Location = new System.Drawing.Point(76, 23);
            this.cb_flipX.Name = "cb_flipX";
            this.cb_flipX.Size = new System.Drawing.Size(52, 17);
            this.cb_flipX.TabIndex = 3;
            this.cb_flipX.Text = "Flip X";
            this.cb_flipX.UseVisualStyleBackColor = true;
            this.cb_flipX.CheckedChanged += new System.EventHandler(this.cb_flipX_CheckedChanged);
            // 
            // pct_preview
            // 
            this.pct_preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pct_preview.Location = new System.Drawing.Point(6, 19);
            this.pct_preview.Name = "pct_preview";
            this.pct_preview.Size = new System.Drawing.Size(64, 64);
            this.pct_preview.TabIndex = 1;
            this.pct_preview.TabStop = false;
            // 
            // gb_StartScreenPreview
            // 
            this.gb_StartScreenPreview.BackColor = System.Drawing.SystemColors.Control;
            this.gb_StartScreenPreview.Controls.Add(this.lbl_rightclick);
            this.gb_StartScreenPreview.Controls.Add(this.lbl_leftclick);
            this.gb_StartScreenPreview.Controls.Add(this.pct_startScreen_btm);
            this.gb_StartScreenPreview.Controls.Add(this.pct_startScreen_top);
            this.gb_StartScreenPreview.Controls.Add(this.rb_luigi);
            this.gb_StartScreenPreview.Controls.Add(this.rb_mario);
            this.gb_StartScreenPreview.Controls.Add(this.pct_background);
            this.gb_StartScreenPreview.Enabled = false;
            this.gb_StartScreenPreview.Location = new System.Drawing.Point(291, 27);
            this.gb_StartScreenPreview.Name = "gb_StartScreenPreview";
            this.gb_StartScreenPreview.Size = new System.Drawing.Size(518, 467);
            this.gb_StartScreenPreview.TabIndex = 3;
            this.gb_StartScreenPreview.TabStop = false;
            this.gb_StartScreenPreview.Text = "Start Screen";
            // 
            // lbl_rightclick
            // 
            this.lbl_rightclick.AutoSize = true;
            this.lbl_rightclick.BackColor = System.Drawing.Color.Black;
            this.lbl_rightclick.ForeColor = System.Drawing.Color.White;
            this.lbl_rightclick.Location = new System.Drawing.Point(392, 46);
            this.lbl_rightclick.Name = "lbl_rightclick";
            this.lbl_rightclick.Size = new System.Drawing.Size(117, 13);
            this.lbl_rightclick.TabIndex = 7;
            this.lbl_rightclick.Text = "Right Click - Select Tile";
            // 
            // lbl_leftclick
            // 
            this.lbl_leftclick.AutoSize = true;
            this.lbl_leftclick.BackColor = System.Drawing.Color.Black;
            this.lbl_leftclick.ForeColor = System.Drawing.Color.White;
            this.lbl_leftclick.Location = new System.Drawing.Point(392, 32);
            this.lbl_leftclick.Name = "lbl_leftclick";
            this.lbl_leftclick.Size = new System.Drawing.Size(115, 13);
            this.lbl_leftclick.TabIndex = 6;
            this.lbl_leftclick.Text = "Left click    - Paste Tile";
            // 
            // pct_startScreen_btm
            // 
            this.pct_startScreen_btm.BackColor = System.Drawing.Color.Black;
            this.pct_startScreen_btm.Location = new System.Drawing.Point(144, 243);
            this.pct_startScreen_btm.Name = "pct_startScreen_btm";
            this.pct_startScreen_btm.Size = new System.Drawing.Size(224, 16);
            this.pct_startScreen_btm.TabIndex = 5;
            this.pct_startScreen_btm.TabStop = false;
            this.pct_startScreen_btm.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pct_startScreen_btm_Click);
            // 
            // pct_startScreen_top
            // 
            this.pct_startScreen_top.BackColor = System.Drawing.Color.Black;
            this.pct_startScreen_top.Location = new System.Drawing.Point(144, 227);
            this.pct_startScreen_top.Name = "pct_startScreen_top";
            this.pct_startScreen_top.Size = new System.Drawing.Size(224, 16);
            this.pct_startScreen_top.TabIndex = 1;
            this.pct_startScreen_top.TabStop = false;
            this.pct_startScreen_top.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pct_startScreen_top_Click);
            // 
            // rb_luigi
            // 
            this.rb_luigi.AutoSize = true;
            this.rb_luigi.Location = new System.Drawing.Point(436, -1);
            this.rb_luigi.Name = "rb_luigi";
            this.rb_luigi.Size = new System.Drawing.Size(75, 17);
            this.rb_luigi.TabIndex = 4;
            this.rb_luigi.Text = " Luigi Tiles";
            this.rb_luigi.UseVisualStyleBackColor = true;
            // 
            // rb_mario
            // 
            this.rb_mario.AutoSize = true;
            this.rb_mario.Checked = true;
            this.rb_mario.Location = new System.Drawing.Point(360, -1);
            this.rb_mario.Name = "rb_mario";
            this.rb_mario.Size = new System.Drawing.Size(76, 17);
            this.rb_mario.TabIndex = 2;
            this.rb_mario.TabStop = true;
            this.rb_mario.Text = "Mario Tiles";
            this.rb_mario.UseVisualStyleBackColor = true;
            this.rb_mario.CheckedChanged += new System.EventHandler(this.rb_mario_CheckedChanged);
            // 
            // pct_background
            // 
            this.pct_background.BackColor = System.Drawing.Color.Black;
            this.pct_background.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pct_background.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pct_background.Location = new System.Drawing.Point(3, 16);
            this.pct_background.Name = "pct_background";
            this.pct_background.Size = new System.Drawing.Size(512, 448);
            this.pct_background.TabIndex = 0;
            this.pct_background.TabStop = false;
            // 
            // ms_main
            // 
            this.ms_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_file,
            this.tsmi_patch,
            this.tsmi_display});
            this.ms_main.Location = new System.Drawing.Point(0, 0);
            this.ms_main.Name = "ms_main";
            this.ms_main.Size = new System.Drawing.Size(814, 24);
            this.ms_main.TabIndex = 8;
            this.ms_main.Text = "menuStrip1";
            // 
            // tsmi_file
            // 
            this.tsmi_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_loadGFX,
            this.tsmi_loadPalette,
            this.tsmi_LoadImage});
            this.tsmi_file.Name = "tsmi_file";
            this.tsmi_file.Size = new System.Drawing.Size(37, 20);
            this.tsmi_file.Text = "File";
            // 
            // tsmi_loadGFX
            // 
            this.tsmi_loadGFX.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_loadGFX0F,
            this.tsmi_loadGFX10});
            this.tsmi_loadGFX.Name = "tsmi_loadGFX";
            this.tsmi_loadGFX.Size = new System.Drawing.Size(163, 22);
            this.tsmi_loadGFX.Text = "Load GFX";
            // 
            // tsmi_loadGFX0F
            // 
            this.tsmi_loadGFX0F.Name = "tsmi_loadGFX0F";
            this.tsmi_loadGFX0F.Size = new System.Drawing.Size(139, 22);
            this.tsmi_loadGFX0F.Text = "Load GFX 0F";
            this.tsmi_loadGFX0F.Click += new System.EventHandler(this.tsmi_loadGFX0F_Click);
            // 
            // tsmi_loadGFX10
            // 
            this.tsmi_loadGFX10.Name = "tsmi_loadGFX10";
            this.tsmi_loadGFX10.Size = new System.Drawing.Size(139, 22);
            this.tsmi_loadGFX10.Text = "Load GFX 10";
            this.tsmi_loadGFX10.Click += new System.EventHandler(this.tsmi_loadGFX10_Click);
            // 
            // tsmi_loadPalette
            // 
            this.tsmi_loadPalette.Enabled = false;
            this.tsmi_loadPalette.Name = "tsmi_loadPalette";
            this.tsmi_loadPalette.Size = new System.Drawing.Size(163, 22);
            this.tsmi_loadPalette.Text = "Load Palette";
            this.tsmi_loadPalette.Click += new System.EventHandler(this.tsmi_loadPalette_Click);
            // 
            // tsmi_LoadImage
            // 
            this.tsmi_LoadImage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_FromROM,
            this.tsmi_FromEditFile});
            this.tsmi_LoadImage.Enabled = false;
            this.tsmi_LoadImage.Name = "tsmi_LoadImage";
            this.tsmi_LoadImage.Size = new System.Drawing.Size(163, 22);
            this.tsmi_LoadImage.Text = "Load Start Image";
            // 
            // tsmi_FromROM
            // 
            this.tsmi_FromROM.Name = "tsmi_FromROM";
            this.tsmi_FromROM.Size = new System.Drawing.Size(132, 22);
            this.tsmi_FromROM.Text = "From ROM";
            this.tsmi_FromROM.Click += new System.EventHandler(this.tsmi_FromROM_Click);
            // 
            // tsmi_FromEditFile
            // 
            this.tsmi_FromEditFile.Name = "tsmi_FromEditFile";
            this.tsmi_FromEditFile.Size = new System.Drawing.Size(132, 22);
            this.tsmi_FromEditFile.Text = "From File";
            this.tsmi_FromEditFile.Click += new System.EventHandler(this.tsmi_FromEditFile_Click);
            // 
            // tsmi_patch
            // 
            this.tsmi_patch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_generate_code,
            this.tsmi_patch_rom,
            this.tsmi_createFile});
            this.tsmi_patch.Name = "tsmi_patch";
            this.tsmi_patch.Size = new System.Drawing.Size(49, 20);
            this.tsmi_patch.Text = "Patch";
            // 
            // tsmi_generate_code
            // 
            this.tsmi_generate_code.Name = "tsmi_generate_code";
            this.tsmi_generate_code.Size = new System.Drawing.Size(182, 22);
            this.tsmi_generate_code.Text = "Generate ASM Patch";
            this.tsmi_generate_code.Click += new System.EventHandler(this.tsmi_generate_code_Click);
            // 
            // tsmi_patch_rom
            // 
            this.tsmi_patch_rom.Name = "tsmi_patch_rom";
            this.tsmi_patch_rom.Size = new System.Drawing.Size(182, 22);
            this.tsmi_patch_rom.Text = "Patch ROM directly";
            this.tsmi_patch_rom.Click += new System.EventHandler(this.tsmi_patch_rom_Click);
            // 
            // tsmi_createFile
            // 
            this.tsmi_createFile.Name = "tsmi_createFile";
            this.tsmi_createFile.Size = new System.Drawing.Size(182, 22);
            this.tsmi_createFile.Text = "Create File";
            this.tsmi_createFile.Click += new System.EventHandler(this.tsmi_createFile_Click);
            // 
            // tsmi_display
            // 
            this.tsmi_display.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_startScreen,
            this.tsmi_tileGrid});
            this.tsmi_display.Name = "tsmi_display";
            this.tsmi_display.Size = new System.Drawing.Size(57, 20);
            this.tsmi_display.Text = "Display";
            // 
            // tsmi_startScreen
            // 
            this.tsmi_startScreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsmi_startScreen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_displayBorder,
            this.tsmi_startGrid,
            this.tsmi_spclRegions});
            this.tsmi_startScreen.Name = "tsmi_startScreen";
            this.tsmi_startScreen.Size = new System.Drawing.Size(136, 22);
            this.tsmi_startScreen.Text = "Start Screen";
            // 
            // tsmi_displayBorder
            // 
            this.tsmi_displayBorder.Checked = true;
            this.tsmi_displayBorder.CheckOnClick = true;
            this.tsmi_displayBorder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmi_displayBorder.Name = "tsmi_displayBorder";
            this.tsmi_displayBorder.Size = new System.Drawing.Size(216, 22);
            this.tsmi_displayBorder.Text = "Display Border";
            this.tsmi_displayBorder.CheckedChanged += new System.EventHandler(this.tsmi_displayBorder_CheckedChanged);
            // 
            // tsmi_startGrid
            // 
            this.tsmi_startGrid.CheckOnClick = true;
            this.tsmi_startGrid.Name = "tsmi_startGrid";
            this.tsmi_startGrid.Size = new System.Drawing.Size(216, 22);
            this.tsmi_startGrid.Text = "Start Screen Grid";
            this.tsmi_startGrid.CheckedChanged += new System.EventHandler(this.tsmi_startGrid_CheckedChanged);
            // 
            // tsmi_spclRegions
            // 
            this.tsmi_spclRegions.CheckOnClick = true;
            this.tsmi_spclRegions.Name = "tsmi_spclRegions";
            this.tsmi_spclRegions.Size = new System.Drawing.Size(216, 22);
            this.tsmi_spclRegions.Text = "Highlight Mario/Luigi Tiles";
            this.tsmi_spclRegions.CheckedChanged += new System.EventHandler(this.tsmi_spclRegions_CheckedChanged);
            // 
            // tsmi_tileGrid
            // 
            this.tsmi_tileGrid.CheckOnClick = true;
            this.tsmi_tileGrid.Enabled = false;
            this.tsmi_tileGrid.Name = "tsmi_tileGrid";
            this.tsmi_tileGrid.Size = new System.Drawing.Size(136, 22);
            this.tsmi_tileGrid.Text = "Tile Grid";
            this.tsmi_tileGrid.CheckedChanged += new System.EventHandler(this.tsmi_tileGrid_CheckedChanged);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            this.displayToolStripMenuItem.Text = "Display";
            // 
            // rb_startscreen
            // 
            this.rb_startscreen.AutoSize = true;
            this.rb_startscreen.Checked = true;
            this.rb_startscreen.Location = new System.Drawing.Point(94, 3);
            this.rb_startscreen.Name = "rb_startscreen";
            this.rb_startscreen.Size = new System.Drawing.Size(140, 17);
            this.rb_startscreen.TabIndex = 9;
            this.rb_startscreen.TabStop = true;
            this.rb_startscreen.Text = "Mario/Luigi Start Screen";
            this.rb_startscreen.UseVisualStyleBackColor = true;
            this.rb_startscreen.CheckedChanged += new System.EventHandler(this.rb_startscreen_changed);
            // 
            // rb_timeup
            // 
            this.rb_timeup.AutoSize = true;
            this.rb_timeup.Location = new System.Drawing.Point(362, 3);
            this.rb_timeup.Name = "rb_timeup";
            this.rb_timeup.Size = new System.Drawing.Size(102, 17);
            this.rb_timeup.TabIndex = 10;
            this.rb_timeup.TabStop = true;
            this.rb_timeup.Text = "Time Up Screen";
            this.rb_timeup.UseVisualStyleBackColor = true;
            this.rb_timeup.CheckedChanged += new System.EventHandler(this.rb_timeup_changed);
            // 
            // rb_gameover
            // 
            this.rb_gameover.AutoSize = true;
            this.rb_gameover.Location = new System.Drawing.Point(240, 3);
            this.rb_gameover.Name = "rb_gameover";
            this.rb_gameover.Size = new System.Drawing.Size(116, 17);
            this.rb_gameover.TabIndex = 11;
            this.rb_gameover.TabStop = true;
            this.rb_gameover.Text = "Game Over Screen";
            this.rb_gameover.UseVisualStyleBackColor = true;
            this.rb_gameover.CheckedChanged += new System.EventHandler(this.rb_gameover_changed);
            // 
            // gb_palette
            // 
            this.gb_palette.Controls.Add(this.pct_paletteView);
            this.gb_palette.Enabled = false;
            this.gb_palette.Location = new System.Drawing.Point(12, 43);
            this.gb_palette.Name = "gb_palette";
            this.gb_palette.Size = new System.Drawing.Size(273, 156);
            this.gb_palette.TabIndex = 13;
            this.gb_palette.TabStop = false;
            this.gb_palette.Text = "Palette";
            // 
            // pct_paletteView
            // 
            this.pct_paletteView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pct_paletteView.Location = new System.Drawing.Point(6, 19);
            this.pct_paletteView.Name = "pct_paletteView";
            this.pct_paletteView.Size = new System.Drawing.Size(256, 128);
            this.pct_paletteView.TabIndex = 4;
            this.pct_paletteView.TabStop = false;
            this.pct_paletteView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pct_paletteView_Click);
            // 
            // gbPosition
            // 
            this.gbPosition.Controls.Add(this.cb_Position);
            this.gbPosition.Controls.Add(this.lblPosText);
            this.gbPosition.Controls.Add(this.tbYPosition);
            this.gbPosition.Location = new System.Drawing.Point(161, 413);
            this.gbPosition.Name = "gbPosition";
            this.gbPosition.Size = new System.Drawing.Size(127, 73);
            this.gbPosition.TabIndex = 14;
            this.gbPosition.TabStop = false;
            this.gbPosition.Text = "Position";
            // 
            // cb_Position
            // 
            this.cb_Position.AutoSize = true;
            this.cb_Position.Location = new System.Drawing.Point(57, 23);
            this.cb_Position.Name = "cb_Position";
            this.cb_Position.Size = new System.Drawing.Size(59, 17);
            this.cb_Position.TabIndex = 2;
            this.cb_Position.Text = "Enable";
            this.cb_Position.UseVisualStyleBackColor = true;
            this.cb_Position.CheckedChanged += new System.EventHandler(this.cbPosition_changed);
            // 
            // lblPosText
            // 
            this.lblPosText.AutoSize = true;
            this.lblPosText.Location = new System.Drawing.Point(5, 44);
            this.lblPosText.Name = "lblPosText";
            this.lblPosText.Size = new System.Drawing.Size(116, 13);
            this.lblPosText.TabIndex = 1;
            this.lblPosText.Text = "Y Position (hex, 1-Byte)";
            // 
            // tbYPosition
            // 
            this.tbYPosition.Enabled = false;
            this.tbYPosition.Location = new System.Drawing.Point(21, 21);
            this.tbYPosition.MaxLength = 2;
            this.tbYPosition.Name = "tbYPosition";
            this.tbYPosition.Size = new System.Drawing.Size(20, 20);
            this.tbYPosition.TabIndex = 0;
            this.tbYPosition.Text = "68";
            this.tbYPosition.TextChanged += new System.EventHandler(this.tbYPosition_changed);
            // 
            // panel_selectScreen
            // 
            this.panel_selectScreen.Controls.Add(this.rb_bonus);
            this.panel_selectScreen.Controls.Add(this.rb_startscreen);
            this.panel_selectScreen.Controls.Add(this.rb_timeup);
            this.panel_selectScreen.Controls.Add(this.rb_gameover);
            this.panel_selectScreen.Location = new System.Drawing.Point(218, 0);
            this.panel_selectScreen.Name = "panel_selectScreen";
            this.panel_selectScreen.Size = new System.Drawing.Size(596, 24);
            this.panel_selectScreen.TabIndex = 15;
            // 
            // rb_bonus
            // 
            this.rb_bonus.AutoSize = true;
            this.rb_bonus.Location = new System.Drawing.Point(470, 3);
            this.rb_bonus.Name = "rb_bonus";
            this.rb_bonus.Size = new System.Drawing.Size(92, 17);
            this.rb_bonus.TabIndex = 12;
            this.rb_bonus.TabStop = true;
            this.rb_bonus.Text = "Bonus Screen";
            this.rb_bonus.UseVisualStyleBackColor = true;
            this.rb_bonus.CheckedChanged += new System.EventHandler(this.rb_Bonus_changed);
            // 
            // MainEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(814, 496);
            this.Controls.Add(this.panel_selectScreen);
            this.Controls.Add(this.gbPosition);
            this.Controls.Add(this.gb_palette);
            this.Controls.Add(this.ms_main);
            this.Controls.Add(this.gb_StartScreenPreview);
            this.Controls.Add(this.gb_preview);
            this.Controls.Add(this.gb_tile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.ms_main;
            this.MaximizeBox = false;
            this.Name = "MainEditor";
            this.Text = "Mario Start Editor v1.2 by TheBiob";
            ((System.ComponentModel.ISupportInitialize)(this.pct_tiles)).EndInit();
            this.gb_tile.ResumeLayout(false);
            this.gb_preview.ResumeLayout(false);
            this.gb_preview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pct_preview)).EndInit();
            this.gb_StartScreenPreview.ResumeLayout(false);
            this.gb_StartScreenPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pct_startScreen_btm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_startScreen_top)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_background)).EndInit();
            this.ms_main.ResumeLayout(false);
            this.ms_main.PerformLayout();
            this.gb_palette.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pct_paletteView)).EndInit();
            this.gbPosition.ResumeLayout(false);
            this.gbPosition.PerformLayout();
            this.panel_selectScreen.ResumeLayout(false);
            this.panel_selectScreen.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pct_tiles;
        private System.Windows.Forms.GroupBox gb_tile;
        private System.Windows.Forms.GroupBox gb_preview;
        private System.Windows.Forms.PictureBox pct_preview;
        private System.Windows.Forms.GroupBox gb_StartScreenPreview;
        private System.Windows.Forms.PictureBox pct_background;
        private System.Windows.Forms.PictureBox pct_startScreen_top;
        private System.Windows.Forms.RadioButton rb_luigi;
        private System.Windows.Forms.RadioButton rb_mario;
        private System.Windows.Forms.PictureBox pct_startScreen_btm;
        private System.Windows.Forms.Button btn_empty;
        private System.Windows.Forms.MenuStrip ms_main;
        private System.Windows.Forms.ToolStripMenuItem tsmi_file;
        private System.Windows.Forms.ToolStripMenuItem tsmi_loadGFX;
        private System.Windows.Forms.ToolStripMenuItem tsmi_loadGFX0F;
        private System.Windows.Forms.ToolStripMenuItem tsmi_loadGFX10;
        private System.Windows.Forms.ToolStripMenuItem tsmi_loadPalette;
        private System.Windows.Forms.ToolStripMenuItem tsmi_patch;
        private System.Windows.Forms.ToolStripMenuItem tsmi_generate_code;
        private System.Windows.Forms.ToolStripMenuItem tsmi_patch_rom;
        private System.Windows.Forms.ToolStripMenuItem tsmi_display;
        private System.Windows.Forms.ToolStripMenuItem tsmi_startScreen;
        private System.Windows.Forms.ToolStripMenuItem tsmi_displayBorder;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.CheckBox cb_flipX;
        private System.Windows.Forms.CheckBox cb_flipY;
        private System.Windows.Forms.ToolStripMenuItem tsmi_startGrid;
        private System.Windows.Forms.ToolStripMenuItem tsmi_spclRegions;
        private System.Windows.Forms.ToolStripMenuItem tsmi_tileGrid;
        private System.Windows.Forms.ToolStripMenuItem tsmi_LoadImage;
        private System.Windows.Forms.ToolStripMenuItem tsmi_FromROM;
        private System.Windows.Forms.ToolStripMenuItem tsmi_FromEditFile;
        private System.Windows.Forms.Label lbl_rightclick;
        private System.Windows.Forms.Label lbl_leftclick;
        private System.Windows.Forms.RadioButton rb_startscreen;
        private System.Windows.Forms.RadioButton rb_timeup;
        private System.Windows.Forms.RadioButton rb_gameover;
        private System.Windows.Forms.GroupBox gb_palette;
        private System.Windows.Forms.PictureBox pct_paletteView;
        private System.Windows.Forms.GroupBox gbPosition;
        private System.Windows.Forms.TextBox tbYPosition;
        private System.Windows.Forms.CheckBox cb_Position;
        private System.Windows.Forms.Label lblPosText;
        private System.Windows.Forms.ToolStripMenuItem tsmi_createFile;
        private System.Windows.Forms.Panel panel_selectScreen;
        private System.Windows.Forms.RadioButton rb_bonus;
    }
}

