﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarioStartEditor {
    class TileData {
        private int tileNumber;
        private int palette;

        private bool Xflipped = false;
        private bool Yflipped = false;

        public TileData(int tile, int palette) {
            this.tileNumber = tile;
            this.palette = palette;
        }

        public TileData(byte tile, byte properties) {
            this.tileNumber = tile;
            if ((properties & 0x80) != 0)
                flipY();
            if ((properties & 0x40) != 0)
                flipX();
            this.palette = (properties & 0x0E) >> 1;
        }

        public TileData(TileData clone) {
            this.tileNumber = clone.getTile();
            this.palette = clone.getPalette();
            this.Xflipped = clone.getXflipped();
            this.Yflipped = clone.getYflipped();
        }

        public void flipX() { this.Xflipped = !this.Xflipped; }
        public void flipY() { this.Yflipped = !this.Yflipped; }

        public void setXflipped(bool Xflipped) { this.Xflipped = Xflipped; }
        public void setYflipped(bool Yflipped) { this.Yflipped = Yflipped; }

        public void setPalette(int newPal) { this.palette = newPal; }

        public int getTile() { return this.tileNumber; }
        public int getPalette() { return this.palette; }

        public bool getXflipped() { return this.Xflipped; }
        public bool getYflipped() { return this.Yflipped; }

        public string tileToHex() {
            return tileNumber.ToString("x2");
        }

        public string propertyToHex() {
            int property = 48;
            property = property | (palette * 2);
            if (Xflipped)
                property = property | 0x40;
            if (Yflipped)
                property = property | 0x80;
            return property.ToString("x2");
        }

        public byte propertyToByte() {
            int property = 48;
            property = property | (palette * 2);
            if (Xflipped)
                property = property | 0x40;
            if (Yflipped)
                property = property | 0x80;
            return (byte)property;
        }

        public TileData Clone() { return new TileData(this); }
    }
}
