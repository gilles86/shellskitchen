﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MarioStartEditor {

    public partial class CodeDisplay : Form {

        private MainEditor editor;

        public CodeDisplay(MainEditor editor) {
            InitializeComponent();
            this.editor = editor;
        }

        public void displayCode(string code) {
            rtb_codeDisplay.Clear();
            rtb_codeDisplay.Text = code;
        }

        private void btn_save_Click(object sender, EventArgs e) {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "ASM File|*.asm";
            sfd.Title = "Save Code";
            if (sfd.ShowDialog() == DialogResult.OK) {
                StreamWriter sw = File.CreateText(sfd.FileName);
                sw.Write(rtb_codeDisplay.Text);
                sw.Close();
            }
        }
    }
}
