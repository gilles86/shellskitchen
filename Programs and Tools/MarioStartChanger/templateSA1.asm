sa1rom

;startscreen
org $0090D1
start_top:

org $009139
start_prop_top:

org $009105
start_btm:

org $00916A
start_prop_btm:
;end

;gameover
org $0090E5
gameover_top:

org $00914D
gameover_prop_top:

org $009119
gameover_btm:

org $00917E
gameover_prop_btm:
;end

;timeup
org $0090EE
timeup_top:

org $009156
timeup_prop_top:

org $009122
timeup_btm:

org $009187
timeup_prop_btm:
;end

;bonus
org $0090F7
bonusgame_top:

org $00912B
bonusgame_btm:

org $00915F
bonusgame_prop_top:

org $009190
bonusgame_prop_btm:
;end

;patch
org $009216
	autoclean JSL Fix
RTS
freecode
Fix:
   LDA $90D1,X
   BMI +
   STA $630A,Y
   LDA #Ypos_1
   STA $6309,Y
+  LDA $9105,X
   BMI +
   STA $630E,Y
   LDA #Ypos_2
   STA $630D,Y
+  RTL
;end