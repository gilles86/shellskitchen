; act as 130

db $42
JMP Mario : JMP Mario : JMP Mario
JMP SpriteV : JMP SpriteH : JMP Return : JMP Return
JMP Mario : JMP Mario : JMP Mario
JMP Mario : JMP Mario

Mario:
    LDY #$00            ;\ Act as air for Mario
    LDA #$25            ;|
    STA $1693|!addr     ;/
SpriteV:
	%check_sprite_kicked_vertical()
	bcs Shatter
	rtl
SpriteH:
	%check_sprite_kicked_horizontal()
	bcs Shatter
	rtl
Shatter:
	%sprite_block_position()
	%shatter_block()
Return:
	rtl

print "A block that shatters when a sprite is thrown at it and is passable for Mario."