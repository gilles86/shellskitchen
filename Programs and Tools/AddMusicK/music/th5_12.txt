;encoding in Shift-JIS
#amk 2

#define SPC_Tag_Language 0

#spc
{
	#author		"ZUN"
	
#if SPC_Tag_Language == 0	;English
	#game		"Touhou Kaikidan"
	#title		"the Last Judgement"
	#comment	"Stage 5 theme"
#endif

#if SPC_Tag_Language == 1	;Japanese (Shift-JIS)
	#game		"�������Y�k�@�` Mystic Square."
	#title		"the Last Judgement"
	#comment	"�T�ʃe�[�}"
#endif
}

#instruments
{
	@0	$AF $6F $00 $06 $00	;@30
	@0	$00 $00 $7F $06 $00	;@31
	@8	$9F $A1 $00 $1E $00	;@32
	@1	$AF $4F $00 $03 $00	;@33
	@1	$00 $00 $7F $03 $00	;@34
	@23 $8C $E0 $70 $07 $00 ;@35
	n1f $FF $F9 $00 $00 $00 ;@36
	n1e $FF $F2 $00 $00 $00 ;@37
}

$EF $1F $D8 $28
$F1 $03 $A0 $01 
$F4 $02

w160
#0 t64
$FA $02 $00
@30 v220  o3 l8
[ [[beaegef+g4abea<b>g<b]]2 >>g<b>f+<b>e<bab4>e<bagbf+bbeaegef+g4abea<b>f+g]2
@31 v200
(002)[e4.b4a8g8f+8g8a4b4e8e4e4.g4f+8d8e2..f+8g8
f+4.d2^8f+8.d8.a8g8.f+8.g8f+4.e2^8] f+4.g2^8
>
(003)[e4.b4a8g8f+8g8a4b4e8e4e4.g4f+8d8e2..f+8g8
f+4.d2^8f+8.d8.a8g8.f+8.g8f+4.e1^8] o4 b16a16g16f+16g16f+16e16<b16
(004)[>e2r8f+8g8a8f+8d16<b8>f+16e4r8f+4g2r8a8b8e8g8f+16d8<a16b4.r4
>e2r8f+8g8a8f+8d16<b8>f+16e4r8f+4g2r8a8b8>e8g8f+16d8] o4 a16b4.r4
(004) o4 b16>e2^8
$FA $02 $01 (002) o3 f+4.g2^8 (003) o4 b16a16g16f+16g16f+16e16<b16 (004) o4 a16b4.r4 (004) o4 b16>e2^8

#1
$FA $02 $00
@32 v200
[[o2 g1^1 b1^1]]4 >
@14 l16 o2 v220
(101)[
[[erbe eebe]]4
[[crgg cgcc]]4
[[drad dadd]]4
[[erbe eebe]]4
]2
(102)[
[[crgg cgcc]]2
ddaddaeberbeebee
]8
$FA $02 $01 (101)2 (102)8

#2
$FA $02 $00
@32 v220
[[o2 c1^1 e1^1]]4
@31 $EE $20 v180
(002) o3 d4.e2^8
(003) o4 g16f+16e16d16g16<a16g16e16 $EE $00
(201)[b2r8>d8e8f+8d8<b16g8>d16<b4r8>d4e2r8f+8g8<b8>e8d16<b8f+16g4.r4
b2r8>d8e8f+8d8<b16g8>d16<b4r8>d4e2r8f+8g8b8>e8d16<b8] f+16g4.r4
(201) g16b2^8
$FA $02 $01 $EE $20
(002) o3 d4.e2^8
(003) o4 g16f+16e16d16g16<a16g16e16 $EE $00
(201) o4 f+16g4.r4
(201) o4 g16b2^8

#3 v255
$FA $02 $00
@33 q76
o4
[[
b4b4b4b8b4.b4b4b8b8b4b4b4b8b4.b4b4b8b8
>c4c4c4c8c4.c4c4c8<a8b4b4b4b8b4.b4b4b8b8
]]2 
q77
(301)[l4
g.a.ag.a.be.f+.ggggg
f+.g.af+.d.f+f+.e2^8f+.g8b16a16g16f+16g16f+16e16<b16
l8
>ebabgaf+gebabgf+e<b>cgf+gef+decgf+gecgc
dagaf+gef+dagaf+f+gaebabgaf+gebabb16a16g16f+16g16f+16e16<b16]
(302)[l16
>e<b>g<b>ef+<b>ee<b>g<b>ef+<b>e
>d<af+>d<af+>e<gbege>e<geg
beg<b>>e<f+<b>ebeg<b>>e<f+<b>e
f+d<a>f+d<abbb>ege<b>ef+g
]4
$FA $02 $01 (301) (302)4

#4
$FA $02 $00
@33 v140 4
[[
e4e4e4e8e4.e4e4e8f+8e4e4e4e8e4.e4e4e8f+8
>g4g4g4g8g4.g4g4f+8d8<e4e4e4e8e4.e4e4e8e8
]]2
v150
(401)[
e4.f+4.f+4e4.f+4.g4c4.d4.e4e4e4e4e4
d4.e4.f+4d4.<b4.>d4d4.<b2^8>d4.e8g16f+16e16d16g16<a16g16e16
l8
b>gf+gef+de<b>gf+ged<bgg>edecd<abg>edec<g>e<g
a>f+ef+de<b>d<a>f+ef+ddef+<b>gf+gef+de<b>gf+gg16f+16e16d16g16<a16g16e16
]
@34 v200
(004) o4 a16b4.r4
(004) o4 b16>e2^8
$FA $02 $01 @33 v150 (401) @34 v200(004) o4 a16b4.r4 (004) o4 b16>e2^8

#5 v255
[[r4]]30 @29 o3 a16a16a16a16 a16a16a16a16

(501)[ [[@21 o4 e8 @29 o3 a8 @21 o4 e8 @29 o3 a16 @21 o4 e16]]7
@29 o3 a16 @21 o4 e16e16e16 @29 o3 a16a16a16a16]
(502)[ [[@21 o4 e8 @29 o3 a8 @21 o4 e8 @29 o3 a16 @21 o4 e16]]6
[[@29 o3 a16 @21 o4 e16e16 @29 o3 a16 @21 o4 e16e16 @29 o3 a8]]2 ]
(501)3 (502) (501)3 (502)
@29 o3 a1^1^1^1 a1^1^1^1 a1a1a1a1a1a1a1
[[@29 o3 a16 @21 o4 e16e16 @29 o3 a16 @21 o4 e16e16 @29 o3 a8]]2
[ [[@21 o4 e4 @29 o3 a4]]14 @29 o3 a4 a4 a16a16a16a16 a16a16a16a16]2

#6 v255 o3
[[@35 q7f a16]]128
(601)[ [[@36 q72 a16 @35 q7f a16a16a16]]13 
@35 q7f a16a16a16a16 @37 q72 a8 @36 q72 a8 @35 q7f a8 @36 q72 a8]
(602)[ [[@36 q72 a16 @35 q7f a16a16a16]]12 @37 q72 a8.a8.a8a8.a8.a8]
(601)3 (602) (601)2
(603)[@36 q72 a16 @35 q7f a16 @37 q72 a16 @35 q7f a16]13
@35 q7f a16a16a16a16 @37 q72 a8 @36 q72 a8 @35 q7f a8 @36 q72 a8
(603)12
@37 q72 a8.a8.a8a8.a8.a8 
a1^1^1^1 a1^1^1^1 a1a1a1a1a1a1a1 a8.a8.a8a8.a8.a8
[ [[@36 q72 a8 @35 q7f a16a16a8a16a16]]14 
@36 q72 a8 @35 q7f a16a16 @36 q72 a8 @35 q7f a16a16 @36 q72 a4a4]2
