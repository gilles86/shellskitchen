#amk 2

; SPC Generator Info
#SPC
{
    #author  "Nobuo Uematsu"
    #title   "Unknown Lands"
    #game    "Final Fantasy V"
    #comment "Ported by Ahrion"
}

#path "UnknownLands"

#samples
{
	#optimized
	"Bass.brr"
	"CH.brr"
	"Crash.brr"
	"Flute.brr"
	"Harp.brr"
	"Kick.brr"
	"Snare Rim.brr"
	"Snare.brr"
	"Strings.brr"
}

#instruments
{
	"Bass.brr"	$FF $EC $7F $04 $7C ; @30
	"CH.brr"	$FF $E0 $7F $02 $46 ; @31
	"Crash.brr"	$FF $E0 $7F $02 $46 ; @32
	"Flute.brr"	$FF $E1 $7F $04 $7C ; @33
	"Harp.brr"	$FF $F2 $7F $03 $00 ; @34
	"Kick.brr"	$FF $E0 $7F $02 $46 ; @35
	"Snare Rim.brr"	$FF $E0 $7F $02 $46 ; @36
	"Snare.brr"	$FF $E0 $7F $02 $46 ; @37
	"Strings.brr"	$FF $E0 $7F $04 $8C ; @38
}


w150
t55


"Chn1 = y13 v201 $FA $03 $20"
"Chn2 = y10 v158"
"Chn3 = y9 v158"
"Chn4 = y8 v171"
"Chn6 = y12 v224 $FA $03 $28"

"Drums1 = v201 $FA $03 $63"
"Drums2 = v185 $FA $03 $2F"
"Drums3 = v139 $FA $03 $2B"

"CH1 = v176"
"CH2 = v165"
"CH3 = v154"
"CH4 = v142"
"CH5 = v121"

"Harp1 = v160"
"Harp2 = v139 $FA $03 $2B"
"Harp3 = v139 $FA $03 $00"
"Harp4 = v114 $FA $03 $40"
"Harp5 = v114 $FA $03 $00"
"Harp6 = v104"
"Harp7 = v94"
"Harp8 = v83"

"VIB = $DE $06 $0B $0D $EA $0C"

; Echo
$EF $1F $19 $19
$F1 $05 $64 $01

$F4 $02



; +---------------------+
; |     Channel #0	|
; |     Flute		|
; +---------------------+

#0
o4

r1^1

/

@33 Chn1 VIB
c2 d4 d+4 a+2. a4 g+1 f2 c+2 c2 c+4 d+4 < a+2 g+4 a+4 > c1 r1 c4 c+4 < a+2 > d+2 c+4 c4 c4 c+4 < a+1. > c4 c+4 < a+2 > f2 d+4 c+4 c4 d+4 < g+1. r1 >>c+2 c4 c+4 c2 < a+1. r2 > c+2 d+2 c+4 c4 c4 c+4 < a+1 a+4 g+8 f+8 g+2 f1 g+4 f+8 f8 f8 f+8 d+1 r4 d+4 f4 c+1 f+4 f4 d+4 c+4 c+1 < b2 > d+2 c+1^1 



; +---------------------+
; |     Channel #1	|
; |     Strings		|
; +---------------------+

#1
o5

@38 Chn2 VIB
(21)[d+16 r16 d+16 r8. d+16 r16 d+16 r16 d+16 r8. d+16 r16]2

/

(21)2
[[c+16 r16 c+16 r8. c+16 r16 c+16 r16 c+16 r8. c+16 r16]]4

d+1^1 < a+1^1^1 > f2 c+2 < a+1^1 d+2. f4 f+4 g+4 a+4 > c4 c+1 f1 f+1^1 c+1 f1 f+1^1 c+1^1 c+2 < b1. a+1^1 a1 b1 > c+1^1 



; +---------------------+
; |     Channel #2	|
; |     Strings		|
; +---------------------+

#2
o4

@38 Chn3 VIB
(31)[g16 r16 g16 r8. g16 r16 g16 r16 g16 r8. g16 r16]2

/

(31)2
[[f16 r16 f16 r8. f16 r16 f16 r16 f16 r8. f16 r16]]4
r2 > c2 < g+2 g2 f+1. f+4 d+4 f1^1 f+1^1 c2. c+4 d+4 f4 f+4 g+4 f1 a+1^1^1 f1 a+1^1^1 g+1^1 a1^1 f+1^1 e1 d+1 f1^1 



; +---------------------+
; |     Channel #3	|
; |     Strings		|
; +---------------------+

#3
o3

r1^1

/

[[r2]]14

@38 Chn4 VIB
g+2 g2 > c+1. c+4 c4 c+1^1 c+1^1 < g+1^1 a+1^1 f+1. f+8 g+8 a+8 > c8 c+1 < a+1 f+1^1 f1 > c+1 < b1^1 > c+1^1 c+1 c+1 c+1^1 



; +---------------------+
; |     Channel #4	|
; |     Harp		|
; +---------------------+

#4
o4

@34 y11
(51)[
Harp1 c16 d+16 g16 > c16 <
Harp2 c16 d+16 g16 > c16 <
Harp3 c16 d+16 g16 > c16 <
Harp4 c16 d+16 g16 > c16 <
Harp5 c16 d+16 g16 > c16 <
Harp6 c16 d+16 g16 > c16 <
Harp7 c16 d+16 g16 > c16 <
Harp8 c16 d+16 g16 > c16 <]

/

(51)

(52)[
Harp1 c+16 f16 g+16 > c+16 <
Harp2 c+16 f16 g+16 > c+16 <
Harp3 c+16 f16 g+16 > c+16 <
Harp4 c+16 f16 g+16 > c+16 <
Harp5 c+16 f16 g+16 > c+16 <
Harp6 c+16 f16 g+16 > c+16 <
Harp7 c+16 f16 g+16 > c+16 <
Harp8 c+16 f16 g+16 > c+16 <]

(53)[
Harp1 < a+16 > c+16 f16 a+16
Harp2 < a+16 > c+16 f16 a+16
Harp3 < a+16 > c+16 f16 a+16
Harp4 < a+16 > c+16 f16 a+16
Harp5 < a+16 > c+16 f16 a+16
Harp6 < a+16 > c+16 f16 a+16
Harp7 < a+16 > c+16 f16 a+16
Harp8 < a+16 > c+16 f16 a+16]

$FA $02 $7B
(52)

$FA $02 $79
(52)
$FA $02 $00
(53)

Harp1 d+16 f+16 a+16 > d+16 <
Harp2 d+16 f+16 a+16 > d+16 <
Harp3 d+16 f+16 a+16 > d+16 <
Harp4 d+16 f+16 a+16 > d+16 <
Harp5 d+16 f+16 a+16 > d+16 <
Harp6 d+16 f+16 a+16 > d+16 <
Harp7 d+16 f+16 a+16 > d+16 <
Harp8 d+16 f+16 a+16 > d+16 <

$FA $02 $7B
(52)
$FA $02 $00

Harp1
[[
< a+16 > c+16 f16 a+16 > c+16 < f16 a+16 > c+16 f16 < a+16 > c+16 f16 a+16 c+16 f16 a+16 > c+16 < a+16 f16 c+16 a+16 f16 c+16 < a+16 > f16 c+16 < a+16 f16 > c+16 < a+16 f16 c+16
< a+16 > c+16 f+16 a+16 > c+16 < f+16 a+16 > c+16 f+16 < a+16 > c+16 f+16 a+16 c+16 f+16 a+16 > c+16 < a+16 f+16 c+16 a+16 f+16 c+16 < a+16 > f+16 c+16 < a+16 f+16 > c+16 < a+16 f+16 c+16
]]2

c+16 f16 g+16 > c+16 f16 < g+16 > c+16 f16 g+16 c+16 f16 g+16 > c+16 < f16 g+16 > c+16 f16 c+16 < g+16 f16 > c+16 < g+16 f16 c+16 g+16 f16 c+16 < g+16 > f16 c+16 < g+16 f16 
< b16 > d+16 f+16 b16 > d+16 < f+16 b16 > d+16 f+16 < b16 > d+16 f+16 b16 d+16 f+16 b16 > d+16 < b16 f+16 d+16 b16 f+16 d+16 < b16 > f+16 d+16 < b16 f+16 > d+16 < b16 f+16 d+16
< a+16 > c+16 f+16 a+16 > c+16 < f+16 a+16 > c+16 f+16 < a+16 > c+16 f+16 a+16 c+16 f+16 a+16 > c+16 < a+16 f+16 c+16 a+16 f+16 c+16 < a+16 > f+16 c+16 < a+16 f+16 > c+16 < a+16 f+16 c+16
< a16 > c+16 e16 a16 > c+16 < e16 a16 > c+16 e16 < a16 > c+16 e16 a16 c+16 e16 a16 b16 f+16 d+16 < b16 > f+16 d+16 < b16 f+16 > d+16 < b16 f+16 d+16 b16 f+16 d+16 < b16 >
c+16 f16 g+16 > c+16 f16 c+16 < g+16 f16 g+16 > c+16 f16 g+16 > c+16 < g+16 f16 c+16 > f16 c+16 < g+16 f16 > c+16 < g+16 f16 c+16 g+16 f16 c+16 < g+16 > f16 c+16 < g+16 f16 



; +---------------------+
; |     Channel #5	|
; |     Bass		|
; +---------------------+

#5
o5

r1^1

/

@30 Chn6 VIB
c1^1 c+1^1 < a+1^1 g+1 > g+2 g2 f+1^1 < a+1^1 > d+1^1 < g+1^1 a+1^1 > f+1^1 < a+1^1 > f+1^1 c+1^1 < b1^1 > f+1^1 < a1 b1 > c+1^1 



; +---------------------+
; |     Channel #6	|
; |     Cymbals		|
; +---------------------+

#6
o5

r1^1

/

[[y10
@32 CH1 a8 @31 CH2 g+16 g+16 CH3 g16 g16 CH4 f+16 f+16 @32 CH5 f8 @31 CH4 e16 e16 @32 CH3 d+8 @31 CH2 d16 d16
@32 CH1 c+8 @31 CH2 d16 d16 CH3 d+16 d+16 CH4 e16 e16 @32 CH5 f8 @31 CH4 f+16 f+16 @32 CH3 g8 @31 CH2 g+16 g+16]]17



; +---------------------+
; |     Channel #7	|
; |     Drums		|
; +---------------------+

#7
o5

r1^1

/

y11
[[
(81)[@35 Drums1 e8 e2.. e8 e4. @37 a2]
@35 e8 e2.. e8 e4. @36 e8^32^96 Drums2 e8^32^96 Drums3 e8^32^96
]]8
(81) 
