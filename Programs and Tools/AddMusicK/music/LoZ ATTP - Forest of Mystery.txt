#amk 2
#instruments
{
	@13 $DB $C0 $B8 $06 $00
	@1  $00 $00 $D5 $03 $00
}
#spc
{
	#author	 "Koji Kondo"
	#game	 "LoZ ATTP / SMW"
	#comment "Ported by tcdw"
	#title	 "Forest of Mystery"
}

; Insert Size:	0x035C bytes
; Ported by:	tcdw
; Length:	0:19
; AddMusic:	AddMusicK
; Channels:	6 (#0,#1,#2,#3,#4,#5)

#0 w200 t29 v200 @31
$ef $ff $00 $00
$f1 $03 $1e $00
$f2 $28 $1e $1e
$f5 $0c $21 $2b $2b $13 $fe $f3 $f9
$de $1e $14 $1e
r16 q2c
o4 b16 q2d
b16 q4d
b8 q2c
b16 b16 b16 
(1)[r16 b16 q2d b16 q4d b4 r16 r16 q2c b16 q2d b16 q4d b8 q2c b16 b16 b16]
r16 o4 b16 q2d
b16 q4d
b4 r16 
v200
@31
$de $1e $14 $1e
r16 q2c
o4 b16 q2d
b16 b16 q2e
b16 q2c
f8 f8
(2)[o4 e16 e16 e4 r16 r16 a16 q2d a16 a16 q2e a16 q2c e8 e16]
^16 o4 d16 d16 d4 r16 
v200 @31 $de $1e $14 $1e
r16 q2c
o4 g16 q2d
g16 g16 q2e
g16 q2c
c8 c16 ^16 d16 e16 f8 e16 f16 g16 q5c ^2 q4c ^2 

#1 @31 v180 $de $1e $14 $1e
[q6c o3 e4. q6e < b8 > f4 c4]2
[v200 $de $1e $14 $1e q6e]
o3 g2 c2 f2 < a+2 
*
g+2 a+2 > c2 ^2 

#2 v180 @31 $de $1e $14 $1e
r16 q2c
o4 g+16 q2d
[g+16 q4d g+8 q2c g+16 g+16 g+16 r16 a16 q2d a16 q4d]
a4 r16 r16 q2c
g+16 q2d
*
a4 r16 
v200 @30 $de $1e $14 $1e
r16 q2c
> b16 q2d
b16 b16 q2e
b16 q4c
f8 q2c
f16 ^16 e16 e32 d32 q4c
e4 r16 r16 q2c
a16 q2d
a16 a16 q2e
a16 q4c
e8 q2c
e16 ^16 d16 d32 c32 q4c
d4 r16 
v200 @30 $de $1e $14 $1e
r16 q2c
o5 g16 q2d
g16 g16 q2e
g16 q2c
c8 c16 ^16 d16 e16 f8 e16 f16 g16 q5c
^2 q4c
^2 

#3 v180 @31 $de $1e $14 $1e
r16 q2c
o4 
[e16 q2d e16 q4d e8 q2c e16 e16 e16 r16 f16 q2d f16 q4d]
f4 r16 r16 q2c
*
f4 r16 
v200 @31 $de $1e $14 $1e
r16 q2c
g16 g16 g16 q5d
g16 q2c
g16 g16 g16 r16 g16 g16 q5c
g16 q2d
g16 q2c
g16 g16 g16 
[r16 f16 f16 f16 q5d f16 q2c f16 f16 f16]2
v200 @31 $de $1e $14 $1e
r16 q2c
o3 g+16 g+16 g+16 q5d
g+16 q2c
g+16 g+16 g+16 r16 a+16 a+16 q5c
a+16 q2d
a+16 q2c
a+16 a+16 a+16 

[r16 o4 g16 g16 g16 q5d
g16 q2c
g16 g16 g16]2

#4 v160 @30
r2 r3 q3d
o5 b48 a48 q7e
b8 r2 r6... q3d
b32 a32 q7e
b8 
v200 @31 $de $1e $14 $1e
r16 q2c
< d16 d16 d16 q5d
d16 q2c
d16 d16 d16 r16 c16 c16 c16 q5d
c16 q2c
c16 c16 c16 r16 c16 c16 c16 q5c
c16 q2d
c16 q2c
c16 c16 r16 < a+16 a+16 a+16 q5c
a+16 q2d
a+16 q2c
a+16 a+16 
v200 @31 $de $1e $14 $1e
r16 q2c
> d+16 d+16 d+16 q5d
d+16 q2c
d+16 d+16 d+16 r16 f16 f16 f16 q5d
f16 q2c
f16 f16 f16 

[r16 c16 c16 c16 q5c
c16 q2d
c16 q2c
c16 c16]2

#5 v130 @31 $de $1e $14 $1e
r=20 q2c
o4 b16 q2d
b16 q4d
b8 q2c
b16 b16 b16 
(1)
r16 o4 b16 q2d
b16 q4d
b4 r48 
v140 @31 $de $1e $14 $1e
r=20 q2c
b16 q2d
b16 b16 q2e
b16 q2c
f8 f8
(2) ^16 o4 d16 d16 d4 r48 
v140 @31 $de $1e $14 $1e
r16 q2c
g16 q2d
g16 g16 q2e
g16 q2c
c8 c16 ^16 d16 e16 f8 e16 f16 g16 q5c ^2 q4c ^2 
