(Please enable word wrap!)

SMW TileEdit is a new misc. text editor, which is more powerful than Smallhacker's version.

 - You don't need to specify where is Graphics folder -- SMW TileEdit automatically loads from ROM and supports both LZ2 and LZ3. But if you want a override the graphics decompression for some reason, put the GFX28.bin, GFX29.bin, GFX2A.bin and GFX2B.bin in this folder.

 - You don't need to edit the Misc. Text again if you ported to a new ROM, you can export/import the tile data using this tool.

 - You don't need to edit tile-per-tile, since there are a option to edit using a TextBox (aka using text).

 - You don't need to worry about ROMs types. LoROM, FastROM, SA-1 ROM and even ExLoROM (8MB ROMs) works here!

 - It should work on XP, Vista and Windows 7 and maybe Linux if you override the graphics loading.

The editor is very easy to work. All tiles is zoomed by 2 (two).

Left click selects a tile.
Right click writes a tile.

Keys 1-8 changes the palette.
X toggles Flip X.
Y toggles Flip Y.
E exports the tiledata.
I imports the tiledata.
S saves the changes to your ROM.

If for some reason you need to override the palette, put the palette file (in YY-CHR format) with name as "fileselect" on tool's folder.

Right now it only edits the File Select, but in future it may get able to edit Overworld Misc. Text (Save menu and Continue/End menu) and if you have any suggestion, free feel to tell me.

~~== Editing with Text ~~==

When you click the "Edit with Text" button, a window will appear, letting you edit with text the tilemap. Simply change the text boxes and it will automatically change the tilemap.

The palette combo box lets you to select which palette will be used. There are 8 available options:
 - Keep
 - Force 0, 1, 2... 7.

Using the option "Keep", the text editor will not change the palette of the tilemap. Using "Force 0", "Force 1", ..., "Force 7", will change the palette to specified color.

The font combo box lets you to select which font will be used. By default, the File Select and other Misc. Texts, it uses a special font. But the problem is, not all letters is available, like F, J, K, X, Q... So, theres another option: Use a alternative font. With this option, the text will use the same font used in Status Bar/other, but that will change for a bit the look of the menu.