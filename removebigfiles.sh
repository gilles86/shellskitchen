#!/bin/bash

# Threshold in bytes (90MB in this example)
SIZE_THRESHOLD=$((90 * 1024 * 1024))

# Find objects over the threshold, excluding LFS pointers by size limit
git rev-list --objects --all |
git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' |
awk -v size=$SIZE_THRESHOLD '$3 > size {print $2}' |
while read object_id; do
    file_path=$(git rev-list --objects --all | grep $object_id | awk '{print $2}' | sort -u)
    if [ ! -z "$file_path" ]; then
        echo "Found large file: $file_path"

        # Backup the large file temporarily outside your repository
        cp --parents "$file_path" /tmp/large_file_backup/

        # Use git filter-branch to remove the file from all branches and commit history
        git filter-branch --force --index-filter \
        "git rm --cached --ignore-unmatch '$file_path'" \
        --prune-empty --tag-name-filter cat -- --all

        # Restore the file in your working directory if it still exists
        if [ -f "/tmp/large_file_backup/$file_path" ]; then
            mkdir -p "$(dirname "$file_path")"
            cp "/tmp/large_file_backup/$file_path" "$file_path"
        fi
    fi
done

# Clean up and compact your repo after the operation
git reflog expire --expire=now --all
git gc --prune=now

# Force push the changes to the remote repository
# Warning: This will overwrite history. Use with caution and inform your team.
# git push origin --force --all
# git push origin --force --tags

echo "Operation completed. Large files greater than 90 MB have been removed from history."