;==============================================
; Mario & Luigi - Selection v2.0 by DiscoTheBat
; This patch replaces "1 Player or 2 Players"
; to
; "Mario Game or Luigi Game"
; Give credit if used.
;==============================================

!base1 = $0000
!base2 = $0000

if read1($00FFD5) == $23
sa1rom
!base1 = $3000
!base2 = $6000
endif

!SingleController = 1	;Set to 1 if you want player 2
	;to be controlled by controller 1

if !SingleController == 1
org $86A0
STZ $0DA0|!base2
LDA $0DA0|!base2
LDX #$00
endif

org $009E0D
STX $0DB3|!base2

org $009E21
LDX $0DB3|!base2

org $04828A
LDY $0DB3|!base2

org $04873B
LDA $0DB3|!base2

org $049D9A
LDA $0DB3|!base2

org $04DBDD
LDA $0DB3|!base2

org $009E5F			;\Used by SRAM Expand
STZ $0DB2|!base2	;/(Not a compatibility problem, but you may want to move the hijack of SRAM patch)

org $048249			;\Hijacked this location because it runs inside SA-1
autoclean JML New	;/IF your hack uses SA-1, means extra speed

org $05B15D	;\Disable Auto-Walk when starting a new game
NOP #3		;/ (since I can't get Luigi to auto-walk)

org $048769	;\If Mario is selected, Luigi won't appear on OW
BRA + : NOP	;/If Luigi is selected, Mario won't appear on OW

org $04876C	;\ Save a few cycles
+			;/

org $04828D	;\Disable Lives Exchanger entirely
BRA $06		;/This will fix an small bug (it's a tiny bug don't worry)

org $00A0BF
autoclean JML New2

org $05B872					;\Change the "1 PLAYER/2 PLAYER" text
incbin MarioLuigiLayer3.bin	;/

freespace noram

New:
PHA					;Store value of 'A'
STZ $0DB2|!base2	;Clear 2-Player Mode flag
STZ $0DD8|!base2	;Do NOT switch for Luigi anymore

LDA $13D9|!base2		;\
CMP #$06				;|
BEQ .NotWrite			;|Stop switching players
CMP #$07				;|
BEQ .NotWrite			;|The game will be truly 1 player only
CMP #$08				;|
BEQ .NotWrite			;/
PLA
JML $048261

.NotWrite
LDA #$03			;\ Keep standing, no need to change...
STA $13D9|!base2	;/
PLA
JML $048261

New2:
LDA $0DBE|!base2
BPL .ContinueEND
LDA #$01
STA $13C9|!base2
LDA #$FF
STA $0DBE|!base2
JML $00A0C4

.ContinueEND
JML $00A0C7
