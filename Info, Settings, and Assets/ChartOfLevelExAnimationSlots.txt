Because non-global ExAnimation has to be set up on a per-level basis, 
whenever you want to use any of the additional animation shown in the demo level,
you'll have to copy over this animation — that is, the ExAnimation slots — into your level.

DON'T JUST IMMEDIATELY COPY OVER EVERY SINGLE SLOT, THOUGH. There are several reason to
be cautious about this, including the fact that some animation will appear over space in BG/FG slots you may use.

__________________________________

ExAnimation slots for level 139 (main demo level) / mostly Alt GFX 61: 

Slot 00: animated one-way UP/DOWN blocks (if you don't use, it'll still show non-animated one-ways)
Slot 01: animated one-way RIGHT/LEFT blocks (same as above)
Slot 02: sprite-only transparent "water," sides and corners
Slot 03: sprite-only transparent "water," top (horizontal)
Slot 04: boost block, shoots you up into air
Slot 05: sprite-only bounce (note)block
Slot 06: ON/OFF block, triggered by independent address (not $14AF)
Slot 07: same as above, but starts OFF 
Slot 08: Animated end goal posts
Slot 09: sprite bullet bill animation
Slot 0A: spinning shell
Slot 0B: tiny coin, uses palette colors C-E, not 3-7 (formerly muncher, same, but now global)
Slot 0C: door appears on trigger (doesn't use alt ExGFX61)
Slot 0D (only found in level 9D): acid or lava, alt. palette colors (BG1)
Slot 0E: rainbow palette cycle, used for disco block etc. (doesn't use alt ExGFX61)
Slot 0F: block that turns normal shells into discos
Slot 10: vanilla berries to put on bushes (leaves green tile behind)
Slot 11: ON/OFF block triggered by p-switches
Slot 12: same as above, but starts OFF
Slot 13: pipe piranha head, acts like (block version) spiny
Slot 14: swooper block
Slot 15 (only found in level 9D, but also relevant castle levels): castle candle flame halo effect (FG3, doesn't use alt ExGFX61)
Slot 16: reusable p-switch block
Slot 17: animated rope, horizontal (297)
Slot 18 (only found in level 9D): Better bubbling castle lava GFX (FG3, doesn't use alt ExGFX61)
Slot 19: sprite-only ON/OFF switch (starts ON)
Slot 1A: same as above, starts OFF
Slot 1B: Yoshi coin flashing palette, for carryable sprite killer (doesn't use alt ExGFX61)
Slot 1C (only found in level 9D): sideways muncher, uses alternate palette colors (C-E) (BG1)
Slot 1D (only found in level 9D): flashing stars for layer 2 BG, uses different palette colors (BG1)
Slot 1E (only found in level 9D): sparkly water, sides and corners (BG1)
Slot 1F: on/off tiny coins, single 8x8 tile
__________________________________

Level 9D has additional animation, and mainly uses ExGFX100 in AN2